package uz.pdp.myticket.utils;

public interface Constants {
    String SUCCESS_SAVE = "Successfully saved!";
    String FAILED_TO_SAVE = "Failed to save!";
    String EXISTS = "Object already exists!";
    String OBJECT_NOT_FOUND = "Object not found!";
    String SUCCESS_EDIT = "Successfully edited";
    String SUCCESS_DELETE = "Successfully deleted!";
    String FAILED_TO_DELETE = "Failed to delete!";
    String FAILED_TO_EDIT = "Failed to edit!";
    String EMPTY_LIST = "List is empty!";
    String ERROR = "Error!";
    String SUCCESS = "Success!";
    String SUCCESS_REFUND="Successfully refunded";
    String FAILED_TO_REFUND="Failed to refund";
    String INVALID_TICKET="Invalid ticket";
    String VALID_TICKET="Valid ticket";
    String DEFAULT_PAGE_SIZE = "5";
    String SUCCESS_LOGIN = "Successfully login!";
}
