package uz.pdp.myticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@SpringBootApplication
public class MyTicketApplication {

    public static void main(String[] args) {

        SpringApplication.run(MyTicketApplication.class, args);


    }

}
