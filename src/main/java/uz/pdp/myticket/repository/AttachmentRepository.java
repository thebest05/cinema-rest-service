package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.myticket.model.Attachment;

import java.util.UUID;

public interface AttachmentRepository extends JpaRepository<Attachment, UUID> {
    Attachment findAttachmentById(UUID id);

}
