package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.myticket.model.UserPermission;

import java.util.Optional;
import java.util.UUID;

public interface PermissionRepository extends JpaRepository<UserPermission, UUID> {

    Optional<UserPermission> findUserPermissionsByName(String name);
}
