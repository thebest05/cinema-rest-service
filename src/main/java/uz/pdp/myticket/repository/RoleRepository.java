package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.myticket.model.Role;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {

    Optional<Role> findRoleByName(String name);
}
