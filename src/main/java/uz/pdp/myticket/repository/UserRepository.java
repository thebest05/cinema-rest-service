package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.myticket.model.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    Optional<User> findByUsername(String username);

    Optional<User> findUserByEmail(@Email String email);

    boolean existsUserByEmailAndUsernameAndPhoneNumber(@Email String email, @Size(min = 2) @NotBlank String username, String phoneNumber);
}
