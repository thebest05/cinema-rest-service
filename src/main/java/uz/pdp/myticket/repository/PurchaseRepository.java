package uz.pdp.myticket.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.myticket.enums.PurchaseStatus;
import uz.pdp.myticket.model.PurchaseHistory;
import uz.pdp.myticket.projection.PurchaseTicketProjection;

import java.util.List;
import java.util.UUID;


public interface PurchaseRepository extends JpaRepository<PurchaseHistory, UUID> {

    @Query(nativeQuery = true,value = "select p.* from purchase_histories p " +
            "join pay_types pt on pt.id = p.pay_type_id " +
            "join attachments a on a.id = pt.logo_id " +
            "join purchase_histories_tickets pht on p.id = pht.purchase_history_id " +
            "join tickets t on t.id = pht.ticket_id where t.id=:ticketId and p.purchase_status='PURCHASE_PAID'")
    PurchaseHistory findPurchaseHistoryByTickets(UUID ticketId);

    @Query(nativeQuery = true,value = "SELECT distinct \n" +
            "       cast(ps.id as varchar) as id,\n" +
            "       ps.purchase_status as status,\n" +
            "       ps.total_amount as totalAmount,\n" +
            "       pt.name as payType,\n" +
            "       cast(t.user_id as varchar) as userId,\n" +
            "       cast(ps.created_at as date) as createdAt\n" +
            " from purchase_histories ps\n" +
            "join pay_types pt on pt.id = ps.pay_type_id\n" +
            "join purchase_histories_tickets pht on ps.id = pht.purchase_history_id\n" +
            "join tickets t on t.id = pht.ticket_id\n" +
            "where t.user_id=:userId and purchase_status ='PURCHASE_PAID'")
    Page<PurchaseTicketProjection> getPurchaseHistoryByUser(UUID userId, Pageable pageable);

    @Query(nativeQuery = true,value = "SELECT distinct \n" +
            "       cast(ps.id as varchar) as id,\n" +
            "       ps.purchase_status as status,\n" +
            "       ps.total_amount as totalAmount,\n" +
            "       pt.name as payType,\n" +
            "       cast(t.user_id as varchar) as userId,\n" +
            "       cast(ps.created_at as date) as createdAt\n" +
            " from purchase_histories ps\n" +
            "join pay_types pt on pt.id = ps.pay_type_id\n" +
            "join purchase_histories_tickets pht on ps.id = pht.purchase_history_id\n" +
            "join tickets t on t.id = pht.ticket_id\n" +
            "where t.user_id=:userId and purchase_status ='PURCHASE_REFUNDED'")
    Page<PurchaseTicketProjection> geRefundPurchaseHistoryByUser(UUID userId, Pageable pageable);

    @Query(nativeQuery = true, value = "select sum(p.totalAmount) from purchase_histories p where p.purchaseStatus = :purchaseStatus")
    Double findPurchaseHistoriesByPurchaseStatus(PurchaseStatus purchaseStatus);

    @Query(nativeQuery = true,value = "SELECT count(pht.ticket_id) from purchase_histories ph " +
            "join purchase_histories_tickets pht on ph.id = pht.purchase_history_id where purchase_status= :status")
    Long countSoldTicket(PurchaseStatus status);


}
