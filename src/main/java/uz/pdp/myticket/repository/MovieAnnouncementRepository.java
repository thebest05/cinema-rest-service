package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.myticket.model.MovieAnnouncement;

import java.util.UUID;

public interface MovieAnnouncementRepository extends JpaRepository<MovieAnnouncement, UUID> {
}
