package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.myticket.model.Attachment;
import uz.pdp.myticket.model.AttachmentContent;
import uz.pdp.myticket.projection.AttachmentContentDataProjection;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
//    @Query(nativeQuery = true,value = "select a from attachment_contents a where a.attachment_id = :id")
    AttachmentContent findAttachmentContentByAttachmentId(UUID id);
    AttachmentContent findAttachmentContentByAttachment(Attachment attachment);
    AttachmentContentDataProjection findAttachmentContentDataByAttachment(Attachment attachment);

}
