package uz.pdp.myticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.myticket.model.ClientCookie;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ClientCookieRepository extends JpaRepository<ClientCookie, UUID> {

    @Query(nativeQuery = true,value = "SELECT cast(ticket_id as varchar) as ticketId from client_cookie  where cookie_id= :cookieId")
    List<UUID> findClientCookieByCookieId(UUID cookieId);

    Optional<ClientCookie> findClientCookieByTicketId(UUID ticketId);

    List<ClientCookie> findClientCookiesByCookieId(UUID cookieId);


}
