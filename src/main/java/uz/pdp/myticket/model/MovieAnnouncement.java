package uz.pdp.myticket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.myticket.template.AbsEntity;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "movie_announcements")
public class MovieAnnouncement extends AbsEntity {

    @ManyToOne
    private Movie movie;

    private boolean isActive;

}
