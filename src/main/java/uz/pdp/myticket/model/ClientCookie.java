package uz.pdp.myticket.model;

import lombok.*;
import org.hibernate.Hibernate;
import uz.pdp.myticket.template.AbsEntity;

import javax.persistence.Entity;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor
@Entity
public class ClientCookie extends AbsEntity {

    private UUID cookieId;
    private UUID ticketId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ClientCookie that = (ClientCookie) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
