package uz.pdp.myticket.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import uz.pdp.myticket.enums.TicketStatus;
import uz.pdp.myticket.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "tickets")
public class Ticket extends AbsEntity {

    private String qrCode;

    private double price;

    @ManyToOne
    private MovieSession movieSession;

    @ManyToOne
    private Seat seat;

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private TicketStatus ticketStatus;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @ManyToMany(mappedBy = "tickets",cascade = CascadeType.ALL)
    List<PurchaseHistory> purchaseHistories;

    private boolean isValid=true;

    public Ticket(String qrCode, double price, MovieSession movieSession, Seat seat, User user, TicketStatus ticketStatus) {
        this.qrCode = qrCode;
        this.price = price;
        this.movieSession = movieSession;
        this.seat = seat;
        this.user = user;
        this.ticketStatus = ticketStatus;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "qrCode='" + qrCode + '\'' +
                ", price=" + price +
                ", movieSession=" + movieSession +
                ", seat=" + seat +
                ", user=" + user +
                ", ticketStatus=" + ticketStatus +
                ", isValid=" + isValid +
                '}';
    }
}
