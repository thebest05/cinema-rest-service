package uz.pdp.myticket.projection;

import java.time.LocalTime;
import java.util.UUID;

public interface TimeSessionProjection {

    UUID getSessionId();

    LocalTime getStartTime();
}
