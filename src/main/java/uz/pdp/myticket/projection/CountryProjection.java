package uz.pdp.myticket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.myticket.model.Cast;
import uz.pdp.myticket.model.Country;

import java.util.UUID;

@Projection(types = {Country.class})
public interface CountryProjection {

    UUID getId();

    String getName();

}
