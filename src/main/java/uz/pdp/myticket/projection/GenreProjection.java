package uz.pdp.myticket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.myticket.model.Country;
import uz.pdp.myticket.model.Genre;

import java.util.UUID;

@Projection(types = {Genre.class})
public interface GenreProjection {

    UUID getId();

    String getName();

}
