package uz.pdp.myticket.projection;

import java.util.UUID;

public interface HallProjection {

    UUID getId();

    String getName();
}
