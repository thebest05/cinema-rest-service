package uz.pdp.myticket.projection;

import java.util.UUID;

public interface RowProjection {

    UUID getId();

    Integer getNumber();
}
