package uz.pdp.myticket.projection;

public interface AttachmentContentDataProjection {
    byte[] getData();
}
