package uz.pdp.myticket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.myticket.model.Country;
import uz.pdp.myticket.model.Distributor;

import java.util.UUID;

@Projection(types = {Distributor.class})
public interface DistributorProjection {

    UUID getId();

    String getName();

}
