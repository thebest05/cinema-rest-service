package uz.pdp.myticket.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.pdp.myticket.model.PurchaseHistory;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Projection(types = {PurchaseHistory.class})
public interface RefundTicketProjection {

    UUID getId();

    String getStatus();

    Double getTotal();

    String getPayType();

    LocalDate getCreatedAt();

    @Value("#{@ticketRepository.getRefundedTickets(target.userId)}")
    List<TicketProjection> getTickets();

}
