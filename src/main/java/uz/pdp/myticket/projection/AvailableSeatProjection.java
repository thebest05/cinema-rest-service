package uz.pdp.myticket.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.myticket.model.Seat;

import java.util.UUID;

@Projection(types = {Seat.class})
public interface AvailableSeatProjection {

    UUID getId();

    String getSeatNumber();

    String getRowNumber();

    Boolean getAvailable();
}
