package uz.pdp.myticket.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.myticket.model.Role;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class JwtResponse {
    private String accessToken;
    private String type = "Bearer ";
    private String refreshToken;
    private UUID userId;
    private String username;
    private String email;
    private Set<Role> roles;
}
