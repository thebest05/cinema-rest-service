package uz.pdp.myticket.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class SmsSenderService {

    @Value("${TWILIO_ACCOUNT_SID}")
    private String accountSid;

    @Value("${TWILIO_AUTH_TOKEN}")
    private String authToken;

    @Value("${TWILIO_PHONE_NUMBER}")
    private String phoneNumberFrom;

    public void sendSms(String phoneNumberTo,String sendMessage){
        Twilio.init(accountSid,authToken);
        Message message = Message.creator(
                new PhoneNumber(phoneNumberTo),
                new PhoneNumber(phoneNumberFrom),
                sendMessage
        ).create();
        System.out.println("Successfully send sms "+message.getSid());
    }
}
