package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import uz.pdp.myticket.config.SpringMailConfig;
import uz.pdp.myticket.dto.MailDto;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Service
public class MailSenderService {

    @Autowired
    SpringTemplateEngine templateEngine;

    @Autowired
    JavaMailSenderImpl mailSender;

    public void sendEmail(String sendTo,String sendContext,String sendSubject) {

        String from = "abduraximovazizjon610@gmail.com";
        String to = sendTo;

        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message, true);
        helper.setSubject(sendSubject);
        helper.setFrom(from);
        helper.setTo(to);
        helper.setText(sendContext, true);
        mailSender.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
    public void sendHtmlMessage(MailDto email) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
        Context context = new Context();
        context.setVariables(email.getProperties());
        helper.setFrom(email.getSendFrom());
        helper.setTo(email.getSendTo());
        helper.setSubject(email.getSendSubject());
        String html = templateEngine.process(email.getSendTemplate(), context);
        helper.setText(html, true);
        mailSender.send(message);
    }
}
