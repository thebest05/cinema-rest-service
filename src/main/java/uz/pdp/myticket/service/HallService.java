package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.pdp.myticket.dto.HallDto;
import uz.pdp.myticket.dto.RowDto;
import uz.pdp.myticket.dto.SeatDto;
import uz.pdp.myticket.model.Hall;
import uz.pdp.myticket.model.PriceCategory;
import uz.pdp.myticket.model.Row;
import uz.pdp.myticket.model.Seat;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.HallAllDataProjection;
import uz.pdp.myticket.repository.HallRepository;
import uz.pdp.myticket.repository.PriceCategoryRepository;
import uz.pdp.myticket.repository.RowRepository;
import uz.pdp.myticket.repository.SeatRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class HallService {

    @Autowired
    HallRepository hallRepository;

    @Autowired
    PriceCategoryRepository priceCategoryRepo;

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    RowRepository rowRepository;

    @Transactional
    public ResponseEntity<?> addHall(HallDto hallDto) {
        Hall hall = hallRepository.save(new Hall(hallDto.getName(), hallDto.getVipAdditionalFeeInPercent()));
        if (hallDto.getRows().size() > 0) {
            for (RowDto row : hallDto.getRows()) {
                Row saveRow = rowRepository.save(new Row(row.getNumber(), hall));
                for (SeatDto seat : row.getSeats()) {
                    PriceCategory priceCategory;
                    try {
                        priceCategory = priceCategoryRepo.findById(seat.getPriceCategoryId()).orElseThrow(() ->
                                new ResourceNotFoundException("price category not found!"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        for (Row row2 : rowRepository.findRowByHallId(hall.getId())) {
                            List<UUID> seatByRowId = seatRepository.findSeatByRowId(row2.getId());
                            seatRepository.deleteAllById(seatByRowId);
                            rowRepository.delete(row2);
                        }
                        hallRepository.delete(hall);
                        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
                    }
                    for (Integer i = seat.getStartSeatNumber(); i <= (seat.getEndSeatNumber() != null ? seat.getEndSeatNumber() : seat.getStartSeatNumber()); i++) {
                        boolean alreadyExist = seatRepository.existsSeatByNumberAndRow(i, saveRow);
                        if (!alreadyExist) {
                            seatRepository.save(new Seat(i, priceCategory, saveRow));
                        } else {
                            for (Row row2 : rowRepository.findRowByHallId(hall.getId())) {
                                List<UUID> seatByRowId = seatRepository.findSeatByRowId(row2.getId());
                                seatRepository.deleteAllById(seatByRowId);
                                rowRepository.delete(row2);
                            }
                            hallRepository.deleteById(hall.getId());
                            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse("Seat "+i+" is already in the "+saveRow.getNumber()+"th row", false, null));
                        }
                    }
                }
            }
        }
        return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, hall));
    }

    public ResponseEntity<?> getAllHalls(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Hall> halls = hallRepository.findAll(pageable);
        return ResponseEntity.ok(new ApiResponse("Success", true, halls));
    }

    public ResponseEntity<?> deleteHall(UUID hallId) {
        try {
            for (Row row : rowRepository.findRowByHallId(hallId)) {
                List<UUID> seatByRowId = seatRepository.findSeatByRowId(row.getId());
                seatRepository.deleteAllById(seatByRowId);
                rowRepository.delete(row);
            }
            hallRepository.deleteById(hallId);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_DELETE, false, null));
        }
    }

    public ResponseEntity<?> editHall(UUID hallId, HallDto hallDto) {
        try {
            Optional<Hall> optionalHall = hallRepository.findById(hallId);
            Hall hall = optionalHall.orElseThrow(() -> new ResourceNotFoundException("Hall not found!"));
            hall.setName(hallDto.getName());
            hall.setVipAdditionalFeeInPercent(hallDto.getVipAdditionalFeeInPercent());
            Hall savedHall = hallRepository.save(hall);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_EDIT, true, savedHall));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

    public ResponseEntity<?> getHallById(UUID hallId) {
        try {
            HallAllDataProjection hallsById = hallRepository.findHallsById(hallId);
            return ResponseEntity.ok(new ApiResponse("Success", true, hallsById));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

}
