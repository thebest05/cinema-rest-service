package uz.pdp.myticket.service;


import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Refund;
import com.stripe.model.checkout.Session;
import com.stripe.param.RefundCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.dto.EmailTicketDto;
import uz.pdp.myticket.dto.MailDto;
import uz.pdp.myticket.enums.PurchaseStatus;
import uz.pdp.myticket.enums.TicketStatus;
import uz.pdp.myticket.model.*;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.PurchaseTicketProjection;
import uz.pdp.myticket.projection.TicketProjection;
import uz.pdp.myticket.repository.*;


import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class PurchaseHistoryService {


    @Value("${STRIPE_SECRET_KEY}")
    String stripeApiKey;

    @Value("${BASE_URL}")
    String baseUrl;

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    PayTypeRepository payTypeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CashBoxRepository cashBoxRepository;

    @Autowired
    RefundChargeFeeRepository refundRepository;

    @Autowired
    MailSenderService mailSenderService;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    @Autowired
    SmsSenderService smsSenderService;

    @Autowired
    AuditorAware<UUID> auditorAware;

    @Transactional
    public ResponseEntity<?> purchaseTicket() {
        try {

            Double commissionFee = (double) 0;
            UUID userId = auditorAware.getCurrentAuditor().orElseThrow(() -> new ResourceNotFoundException("Please login first!"));
            User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found!"));

            List<TicketProjection> ticketList = ticketRepository.getTicketsByUser(user.getId());

            String successURL = baseUrl + "payment/success";
            String failureURL = baseUrl + "payment/failed";

            Stripe.apiKey = stripeApiKey;

            List<SessionCreateParams.LineItem> sessionItemList = new ArrayList<>();
            for (TicketProjection ticket : ticketList) {
                SessionCreateParams.LineItem sessionLineItem = createSessionLineItem(ticket);
                commissionFee += sessionLineItem.getPriceData().getUnitAmount();
                sessionItemList.add(sessionLineItem);
            }

            SessionCreateParams.LineItem lineItem = calculateCommissionFee(commissionFee);
            sessionItemList.add(lineItem);

            SessionCreateParams params = SessionCreateParams.builder()
                    .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
                    .setMode(SessionCreateParams.Mode.PAYMENT)
                    .setClientReferenceId(user.getId().toString())
                    .setCancelUrl(failureURL)
                    .setSuccessUrl(successURL)
                    .addAllLineItem(sessionItemList)
                    .build();

            Session session = Session.create(params);
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, session.getUrl()));
        } catch (Throwable e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

    private SessionCreateParams.LineItem calculateCommissionFee(Double commissionFee) {
         return SessionCreateParams.LineItem.builder()
                .setPriceData(
                        SessionCreateParams.LineItem.PriceData.builder()
                                .setCurrency("USD")
                                .setUnitAmount((long) (((commissionFee + 30) / 0.971) - commissionFee))
                                .setProductData(SessionCreateParams.LineItem.PriceData.ProductData
                                        .builder()
                                        .setName("Commission fee:")
                                        .build())
                                .build()
                )
                .setQuantity(1L)
                .build();
    }

    private SessionCreateParams.LineItem createSessionLineItem(TicketProjection ticket) {
        return SessionCreateParams.LineItem.builder()
                .setPriceData(createPriceData(ticket))
                .setQuantity(1L)
                .build();
    }


    private SessionCreateParams.LineItem.PriceData createPriceData(TicketProjection ticket) {
        return SessionCreateParams.LineItem.PriceData.builder()
                .setCurrency("USD")
                .setUnitAmount((long) (ticket.getPrice() * 100))
                .setProductData(SessionCreateParams.LineItem.PriceData.ProductData
                        .builder()
                        .setName(ticket.getMovieTitle())
                        .setDescription(ticket.getHallName() + " : " + ticket.getStartDate() + " " + ticket.getStartTime())
                        .build())
                .build();
    }

    @Transactional
    public ResponseEntity<?> fulfillOrder(Session session) {
        String clientReferenceId = session.getClientReferenceId();
        String paymentIntent = session.getPaymentIntent();
        try {
            Double totalPrice = Double.valueOf(0);
            User user = userRepository.findById(UUID.fromString(clientReferenceId)).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
            PayType payType = payTypeRepository.findPayTypeByName("Stripe-Card");
            List<Ticket> ticketsByUserId = ticketRepository.getTicketsByUserId(user.getId());
            if (payType != null) {
                for (Ticket ticket : ticketsByUserId) {
                    if (ticket.getTicketStatus().equals(TicketStatus.NEW)) {
                        totalPrice += ticket.getPrice();
                        ticket.setTicketStatus(TicketStatus.PURCHASED);
                        ticket.setQrCode("http://localhost:8080/api/check-ticket/" + ticket.getId());
                        Ticket saveTicket = ticketRepository.save(ticket);
                    }
                }
                PurchaseHistory purchaseHistory = new PurchaseHistory(ticketsByUserId, totalPrice, payType, paymentIntent, PurchaseStatus.PURCHASE_PAID);
                purchaseHistory.setCreatedBy(user.getId());
                purchaseHistory.setUpdatedBy(user.getId());
                purchaseRepository.save(purchaseHistory);
                addMoneyCashbox(totalPrice);
                new Thread(() -> {
                    MailDto mailDto = new MailDto();
                    mailDto.setSendFrom("abduraximovazizjon610@gmail.com");
                    mailDto.setSendTo(user.getEmail());
//                    mailDto.setSendTo("7abror7@gmail.com");
                    mailDto.setSendSubject("Purchase ticket");
                    mailDto.setSendTemplate("email.html");
                    mailDto.setProperties(getAllDataForEmail(ticketsByUserId));
                    try {
                        mailSenderService.sendHtmlMessage(mailDto);
                    } catch (MessagingException e) {
                        e.printStackTrace();
                    }
                }).start();
                new Thread(() -> {
                    List<Ticket> ticketList = ticketsByUserId.stream().filter(distinctByKey(ticket -> ticket.getMovieSession())).collect(Collectors.toList());
                    for (Ticket ticket : ticketList) {
                        LocalDate startDate = ticket.getMovieSession().getStartDate();
                        LocalTime startTime = ticket.getMovieSession().getStartTime();
                        LocalDateTime localDateTime = startDate.atTime(startTime);
                        long between = ChronoUnit.SECONDS.between(LocalDateTime.now(), localDateTime);
                        long delay = (between >= 3600 ? between - 3600 : between) * 1000;
                        Timer timer = new Timer();
                        TimerTask sendSms = new TimerTask() {
                            @Override
                            public void run() {
                                String sendMessage = "Hello " + user.getFirstName() + " " + (user.getLastName() != null ? user.getLastName() : "") +
                                        "  You got a ticket to the movie " + ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle() + ". The movie is less than an hour away from the start." +
                                        "We would like to remind you once again that the movie starts at " + startDate + " at " + startTime;
                                smsSenderService.sendSms(user.getPhoneNumber(), sendMessage);
                            }
                        };
                        timer.schedule(sendSms, delay);
                    }
                }).start();
                return ResponseEntity.ok(new ApiResponse(SUCCESS, true, null));
            } else {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(ERROR, false, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(OBJECT_NOT_FOUND, false, null));
        }
    }

    private void addMoneyCashbox(Double totalPrice) {
        CashBox cashBox = cashBoxRepository.findAll().get(0);
        cashBox.setBalance(cashBox.getBalance() + totalPrice);
        cashBoxRepository.save(cashBox);
    }

    @Transactional
    public ResponseEntity<?> refundTicket(UUID ticketId, Boolean isRefund) {
        if (ticketId == null || ticketId.equals(""))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            Ticket ticket = ticketRepository.findById(ticketId).orElseThrow(() -> new ResourceNotFoundException("Ticket not found!"));
            LocalDate startDate = ticket.getMovieSession().getStartDate();
            LocalTime startTime = ticket.getMovieSession().getStartTime();
            LocalDateTime localDateTime = startDate.atTime(startTime);
            long between = ChronoUnit.MINUTES.between(LocalDateTime.now(), localDateTime);
            int waitingTime = 3600;
            List<Ticket> tickets = new ArrayList<>();
            tickets.add(ticket);
            PurchaseHistory purchaseHistory = purchaseRepository.findPurchaseHistoryByTickets(ticket.getId());
            if (between < 0) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("This session already is over!", false, null));
            } else if (between <= waitingTime) {
                if (isRefund) {
                    getSaveTicket(ticket);
                    purchaseRepository.save(new PurchaseHistory(tickets, 0.0, purchaseHistory.getPayType(), null, PurchaseStatus.PURCHASE_REFUNDED));
                    tickets.clear();
                    String sendSubject = "Ticket refund";
                    String sendContext = "<p><b>Your ticket for the \"" + ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle() + "\" movie, which starts at " + ticket.getMovieSession().getStartTime() + " on " + ticket.getMovieSession().getStartDate() + ", has been canceled.</b></p>";
                    new Thread(() -> {
                        mailSenderService.sendEmail(ticket.getUser().getEmail(), sendContext, sendSubject);
                    }).start();
                    return ResponseEntity.ok(new ApiResponse(SUCCESS_REFUND, true, null));
                } else {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("You can return your ticket but your money will not be refunded because the movie is an hour away from the start.", false, null));
                }
            } else if (between > waitingTime) {
                return addRefundTicketPurchaseHistory(isRefund, ticket, between, tickets, purchaseHistory);
            } else {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(ERROR, false, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

    private ResponseEntity<ApiResponse> addRefundTicketPurchaseHistory(Boolean isRefund, Ticket ticket, long between, List<Ticket> tickets, PurchaseHistory purchaseHistory) throws StripeException {
        Integer interval = Math.toIntExact(between);
        double refundPercent = refundRepository.getPercent(interval) != null ? refundRepository.getPercent(interval) : 0;
        double additionalFee = ticket.getPrice() * refundPercent / 1000;
        double refundedTicketPrice = ticket.getPrice() - additionalFee;
        if (!isRefund) {
            return ResponseEntity.ok(new ApiResponse("You will be charged a " + refundPercent + "% commission and you will be refunded $ " + refundedTicketPrice + " for returning your ticket on " + LocalDate.now() + " " + LocalTime.now() + ". Are you sure you want to return your ticket?", true, null));
        } else {

            if (purchaseHistory.getPayType().getName().equals("Stripe-Card")) {
                Stripe.apiKey = stripeApiKey;
                RefundCreateParams params = RefundCreateParams
                        .builder()
                        .setPaymentIntent(purchaseHistory.getPaymentIntentId())
                        .setAmount((long) refundedTicketPrice * 100)
                        .build();
                Refund refund = Refund.create(params);
                if (refund.getStatus().equals("succeeded")) {
                    cashWithdrawal(refundedTicketPrice);
                    Ticket saveTicket = getSaveTicket(ticket);
                    tickets.clear();
                    tickets.add(saveTicket);
                    purchaseRepository.save(new PurchaseHistory(tickets, refundedTicketPrice, purchaseHistory.getPayType(), null, PurchaseStatus.PURCHASE_REFUNDED));
                    tickets.clear();
                    String sendSubject = "Ticket refund";
                    String sendContext = "<p><b>Your ticket for the \"" + ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle() + "\" movie, which starts at " + ticket.getMovieSession().getStartTime() + " on " + ticket.getMovieSession().getStartDate() + ", has been canceled. You have been refunded $ " + refundedTicketPrice + ".</b></p>";
                    new Thread(() -> {
                        mailSenderService.sendEmail(ticket.getUser().getEmail(), sendContext, sendSubject);
                    }).start();
                    return ResponseEntity.ok(new ApiResponse("You were charged a " + refundPercent + "% commission for returning your ticket on " + LocalDate.now() + " " + LocalTime.now() + " and you were refunded $ " + refundedTicketPrice + ". Ticket return was successful.", true, null));
                } else {
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(ERROR, false, null));
                }

            } else {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_REFUND, false, null));
            }
        }
    }

    private Ticket getSaveTicket(Ticket ticket) {
        ticket.setTicketStatus(TicketStatus.REFUNDED);
        return ticketRepository.save(ticket);
    }

    private void cashWithdrawal(double refundedTicketPrice) {
        CashBox cashBox = cashBoxRepository.findAll().get(0);
        cashBox.setBalance(cashBox.getBalance() - refundedTicketPrice);
        cashBoxRepository.save(cashBox);
    }


    public Map<String, Object> getAllDataForEmail(List<Ticket> ticketList) {

        Map<String, Object> properties = new HashMap<>();

        List<EmailTicketDto> emailTicketDtoList = new ArrayList<>();

        for (Ticket ticket : ticketList) {
            EmailTicketDto emailTicketDto = new EmailTicketDto();
            emailTicketDto.setId(ticket.getId());
            emailTicketDto.setMovieTitle(ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle());
            if (ticket.getMovieSession().getMovieAnnouncement().getMovie().getPosterImg() != null) {
                emailTicketDto.setMoviePosterImage(ticket.getMovieSession().getMovieAnnouncement().getMovie().getPosterImg().getId());
            }
            emailTicketDto.setHallName(ticket.getMovieSession().getHall().getName());
            emailTicketDto.setPrice(ticket.getPrice());
            emailTicketDto.setRowNumber(ticket.getSeat().getRow().getNumber());
            emailTicketDto.setSeatNumber(ticket.getSeat().getNumber());
            emailTicketDto.setPurchaseDate(ticket.getCreatedAt().toLocalDateTime().toLocalDate());
            emailTicketDto.setStartDate(ticket.getMovieSession().getStartDate());
            emailTicketDto.setStartTime(ticket.getMovieSession().getStartTime());
            emailTicketDto.setQrCode(ticket.getQrCode());
            emailTicketDtoList.add(emailTicketDto);
        }
        properties.put("ticketList", emailTicketDtoList);
        return properties;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    public ResponseEntity<?> getPurchasedTicket(Integer page, Integer size) {
        try {
            UUID userId = auditorAware.getCurrentAuditor().orElseThrow(() -> new ResourceNotFoundException("Please login first!"));
            Pageable pageable= PageRequest.of(page,size);
            Page<PurchaseTicketProjection> purchaseHistoryByUser = purchaseRepository.getPurchaseHistoryByUser(userId,pageable);
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, purchaseHistoryByUser));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

    public ResponseEntity<?> getRefundTicket(Integer page, Integer size) {
        try {
            UUID userId = auditorAware.getCurrentAuditor().orElseThrow(() -> new ResourceNotFoundException("Please login first!"));
            Pageable pageable= PageRequest.of(page,size);
            Page<PurchaseTicketProjection> purchaseHistoryByUser = purchaseRepository.geRefundPurchaseHistoryByUser(userId,pageable);
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, purchaseHistoryByUser));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }
}
