package uz.pdp.myticket.service;


import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.*;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.dto.TicketDto;
import uz.pdp.myticket.enums.TicketStatus;
import uz.pdp.myticket.model.*;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.TicketProjection;
import uz.pdp.myticket.repository.*;
import uz.pdp.myticket.service.qrCodeService.QRCodeServiceImpl;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.List;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class TicketService {

    @Autowired
    TicketRepository ticketRepo;

    @Autowired
    MovieSessionRepository movieSessionRepo;

    @Autowired
    SeatRepository seatRepo;

    @Autowired
    PurchaseWaitingTimeRepository timeRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AttachmentContentRepository contentRepository;

    @Autowired
    QRCodeServiceImpl qrCodeService;

    @Autowired
    AuditorAware<UUID> auditorAware;

    @Autowired
    ClientCookieRepository cookieRepository;

    public ResponseEntity<?> generateTicket(TicketDto ticketDto, HttpServletResponse response, HttpServletRequest request) {
        try {

            UUID seatId = ticketDto.getSeatId();
            UUID sessionId = ticketDto.getSessionId();

            MovieSession movieSession = movieSessionRepo.findById(sessionId)
                    .orElseThrow(() -> new ResourceNotFoundException("Movie session not found!"));

            Seat seat = seatRepo.findById(seatId)
                    .orElseThrow(() -> new ResourceNotFoundException("Seat not found!"));

            Boolean existsTicket = ticketRepo.existsTicket(seat.getId(), movieSession.getId());

            if (existsTicket) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("This place has been booked before",false,null));
            }else{

                Double ticketPrice = ticketRepo.calculateTicketPrice(seat.getId(), movieSession.getId());
                Ticket ticket = new Ticket();
                ticket.setTicketStatus(TicketStatus.NEW);
                ticket.setPrice(ticketPrice);
                ticket.setSeat(seat);
                ticket.setMovieSession(movieSession);
                UUID userId =null;
                Optional<UUID> currentAuditor = auditorAware.getCurrentAuditor();
                if (currentAuditor.isPresent()) {
                    userId=currentAuditor.get();
                }
                if (userId != null) {
                    User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
                    ticket.setUser(user);
                }
                Ticket ticket1 = ticketRepo.save(ticket);
                if (ticket1.getUser() == null) {
                    String cookieIdValue = UUID.randomUUID().toString();
                    Cookie[] cookies = request.getCookies();
                    if (cookies != null) {
                        for (Cookie cookie : cookies) {
                            if (cookie.getName().equals("cart_tickets")){
                                UUID cookieId = UUID.fromString(cookie.getValue());
                                cookieRepository.save(new ClientCookie(cookieId,ticket1.getId()));
                                break;
                            }else{
                                createCookieForTicket(response, cookieIdValue);
                                cookieRepository.save(new ClientCookie(UUID.fromString(cookieIdValue),ticket1.getId()));
                            }
                        }
                    }else {
                        createCookieForTicket(response, cookieIdValue);
                        cookieRepository.save(new ClientCookie(UUID.fromString(cookieIdValue),ticket1.getId()));
                    }
                }
                waitingPurchasedTime(ticket1);
                return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE,true,null));
            }
        } catch (Throwable e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(),false,null));
        }
    }

    private void createCookieForTicket(HttpServletResponse response, String cookieIdValue) {
        Cookie cookie1 = new Cookie("cart_tickets", cookieIdValue);
        cookie1.setMaxAge(3*60*60);
        cookie1.setPath("/");
        cookie1.setDomain("localhost");
        response.addCookie(cookie1);
    }

    private void waitingPurchasedTime(Ticket ticket1) {
        Timer timer = new Timer();
        TimerTask deleteTicket = new TimerTask() {
            @Override
            public void run() {
                Ticket savedTicket = ticketRepo.findById(ticket1.getId())
                        .orElseThrow(() -> new ResourceNotFoundException("Ticket not found!"));

                if (savedTicket.getTicketStatus().equals(TicketStatus.NEW)) {
                    Optional<ClientCookie> clientCookieByTicketId = cookieRepository.findClientCookieByTicketId(ticket1.getId());
                    clientCookieByTicketId.ifPresent(clientCookie -> cookieRepository.delete(clientCookie));
                    ticketRepo.delete(savedTicket);
                    System.out.println("TICKET DELETED");
                }
            }
        };
        Integer waitingTime = timeRepository.findFirstByUpdatedAt();
        if (waitingTime != null) {
            timer.schedule(deleteTicket, waitingTime * 60 * 1000);
        }else {
            timer.schedule(deleteTicket, 30 * 60 * 1000);
        }
    }

    public ResponseEntity<?> deleteTicket(UUID id) {
        if (id == null || id.equals("")) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR,false,null));
        try {
            Ticket ticket = ticketRepo.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ticket not found!"));
            ticketRepo.delete(ticket);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE,true,null));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_DELETE,false,null));
        }
    }

    public ResponseEntity<?> getAllTickets(Integer page, Integer size) {
        try{
            List<TicketProjection> ticketsAll = ticketRepo.findTicketsAll();
            Pageable pageable= PageRequest.of(page,size);
            Page<TicketProjection> ticketProjections = new PageImpl<>(ticketsAll,pageable,ticketsAll.size());
            return ResponseEntity.ok(new ApiResponse(SUCCESS,true,ticketProjections));
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(EMPTY_LIST,false,true));
        }
    }


    public ResponseEntity<?> getMYTicket(Integer page, Integer size) {
        try {
            UUID userId = auditorAware.getCurrentAuditor().orElseThrow(() -> new ResourceNotFoundException("Please login first!"));
            User user = userRepository.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found!"));
            List<TicketProjection> tickets = ticketRepo.getTicketsByUser(user.getId());
            Pageable pageable=PageRequest.of(page,size);
            Page<TicketProjection> ticketProjections = new PageImpl<>(tickets,pageable,tickets.size());
            return ResponseEntity.ok(new ApiResponse(SUCCESS,true,ticketProjections));

        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(),false,null));
        }


    }

    public ResponseEntity<?> downloadTicket(UUID ticketId) {
        if (ticketId == null || ticketId.equals("")) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR,false,null));
        try {
            Ticket ticket = ticketRepo.findById(ticketId).orElseThrow(() -> new ResourceNotFoundException("Ticket not found!"));
            AttachmentContent contentByAttachment = contentRepository.findAttachmentContentByAttachment(ticket.getMovieSession().getMovieAnnouncement().getMovie().getPosterImg());
            PdfWriter pdfWriter = new PdfWriter("src/main/resources/ticket.pdf");
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            pdfDocument.addNewPage();
            Document document = new Document(pdfDocument);
            Paragraph paragraph = new Paragraph("Ticket");
            paragraph.setTextAlignment(TextAlignment.CENTER);
            paragraph.setFontSize(36);
            document.add(paragraph);
            document.setHorizontalAlignment(HorizontalAlignment.CENTER);

            float[] k ={ 300F,500F,100F};
            Table table=new Table(k);


            ImageData data = ImageDataFactory.create(contentByAttachment.getData());
            Image movieImage = new Image(data);
            movieImage.setMaxHeight(300);

            table.addCell(new Cell().add(movieImage));
            String content="Movie name: "+ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle()+"\n"+
                    "Hall: "+ticket.getSeat().getRow().getHall().getName()+" | Row: "+ticket.getSeat().getRow().getNumber()+" | Seat: "+ticket.getSeat().getNumber()+"\n"+
                    "Price: "+ticket.getPrice()+" $\n"+
                    "Start date: "+ticket.getMovieSession().getStartDate()+"\n"+
                    "Start time: "+ticket.getMovieSession().getStartTime();
            table.addCell(content);

            byte[] generateQRCode = qrCodeService.generateQRCode(ticket.getQrCode(), 100, 100);
            ImageData data1 = ImageDataFactory.create(generateQRCode);
            Image qrCode = new Image(data1);
            qrCode.setMaxHeight(300);
            table.addCell(qrCode);

            remove(table);
            document.add(table);
            document.close();
            pdfDocument.close();
            pdfWriter.close();

            File file=new File("src/main/resources/ticket.pdf");
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] downloadData = new byte[(int) file.length()];
            fileInputStream.read(downloadData);
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_PDF)
                    .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; filename=\""+file.getName().substring(0,file.getName().length()-4)
                            +ticket.getMovieSession().getMovieAnnouncement().getMovie().getTitle()
                            +ticket.getSeat().getNumber()+
                            file.getName().substring(file.getName().length()-4)+"\"")
                    .body(new ByteArrayResource(downloadData));

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
    public static void remove(Table table){
        for (IElement iElement : table.getChildren()) {
            ((Cell)iElement).setBorder(Border.NO_BORDER);
        }
    }
}
