package uz.pdp.myticket.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.dto.RowDto;
import uz.pdp.myticket.dto.SeatDto;
import uz.pdp.myticket.model.Hall;
import uz.pdp.myticket.model.PriceCategory;
import uz.pdp.myticket.model.Row;
import uz.pdp.myticket.model.Seat;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.RowAllDataProjection;
import uz.pdp.myticket.projection.RowProjection;
import uz.pdp.myticket.repository.HallRepository;
import uz.pdp.myticket.repository.PriceCategoryRepository;
import uz.pdp.myticket.repository.RowRepository;
import uz.pdp.myticket.repository.SeatRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class RowService {

    @Autowired
    RowRepository rowRepository;

    @Autowired
    HallRepository hallRepository;

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    PriceCategoryRepository priceCategoryRepo;

    public ResponseEntity<?> getAllRows(UUID hallId, Integer page, Integer size) {
        if (hallId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        } else {
            Pageable pageable = PageRequest.of(page, size);
            Page<RowProjection> rowList = rowRepository.findRowsAllByHallId(hallId, pageable);
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, rowList));
        }
    }

    @Transactional
    public ResponseEntity<?> addRow(UUID hallId, RowDto rowDto) {
        if (hallId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        } else {
            Hall hall;
            try {
                hall = hallRepository.findById(hallId).orElseThrow(() -> new ResourceNotFoundException("Hall not found!"));
                Row row = rowRepository.save(new Row(rowDto.getNumber(), hall));
                if (rowDto.getSeats().size() > 0) {
                    for (SeatDto seat : rowDto.getSeats()) {
                        PriceCategory priceCategory;
                        priceCategory = priceCategoryRepo.findById(seat.getPriceCategoryId()).orElseThrow(() ->
                                new ResourceNotFoundException("price category not found!"));
                        for (Integer i = seat.getStartSeatNumber(); i <= (seat.getEndSeatNumber() != null ? seat.getEndSeatNumber() : seat.getStartSeatNumber()); i++) {
                            boolean alreadyExist = seatRepository.existsSeatByNumberAndRow(i, row);
                            if (!alreadyExist) {
                                seatRepository.save(new Seat(i, priceCategory, row));
                            } else {
                                List<UUID> seatByRowId = seatRepository.findSeatByRowId(row.getId());
                                seatRepository.deleteAllById(seatByRowId);
                                rowRepository.delete(row);
                                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse("Seat "+i+" is already in the "+row.getNumber()+"th row", false, null));
                            }
                        }
                    }
                }
                return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, row));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
            }
        }
    }

    public ResponseEntity<?> deleteRow(UUID rowId) {
        if (rowId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        } else {
            Row row;
            try {
                row = rowRepository.findById(rowId).orElseThrow(() -> new ResourceNotFoundException("Row not found !"));
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
            }
            try {
                List<UUID> seatByRowId = seatRepository.findSeatByRowId(rowId);
                seatRepository.deleteAllById(seatByRowId);
                rowRepository.deleteById(rowId);
                return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_DELETE, false, null));
            }
        }
    }

    public ResponseEntity<?> getRowById(UUID rowId) {
        if (rowId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        } else {
            try {
                RowAllDataProjection row = rowRepository.findRowsById(rowId);
                return ResponseEntity.ok(new ApiResponse("Success", true, row));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
            }
        }
    }

    @Transactional
    public ResponseEntity<?> editRow(UUID rowId, RowDto rowDto) {
        if (rowId == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        } else {
            Row row;
            try {
                row = rowRepository.findById(rowId).orElseThrow(() -> new ResourceNotFoundException("Row not found!"));
            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
            }
            try {
                row.setNumber(rowDto.getNumber());
                Row editRow = rowRepository.save(row);
                return ResponseEntity.ok(new ApiResponse(SUCCESS_EDIT, true, editRow));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_EDIT, false, null));
            }

        }
    }

}
