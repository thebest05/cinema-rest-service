package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.dto.MovieSessionDto;
import uz.pdp.myticket.dto.ReservedHallDto;
import uz.pdp.myticket.model.*;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.AvailableSeatProjection;
import uz.pdp.myticket.projection.MovieAllSessionProjection;
import uz.pdp.myticket.repository.*;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.Math.toIntExact;
import static uz.pdp.myticket.utils.Constants.*;

@Service
public class MovieSessionService {

    @Autowired
    MovieSessionRepository movieSessionRepository;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    MovieAnnouncementRepository movieAnnouncementRepository;

    @Autowired
    ExtraTimeBetweenSessionsRepository extraTimeBetweenSessionsRepository;

    @Autowired
    SessionAdditionalFeeInPercentRepository inPercentRepository;

    @Autowired
    HallRepository hallRepository;

    public ResponseEntity<?> getAllMovieSession(LocalDate date, Integer page, Integer size) {
        date = date != null ? date : LocalDate.now();
        if (page == null) page = 0;
        if (size == null) size = 100;
        Pageable pageable = PageRequest.of(page, size);
        Page<MovieAllSessionProjection> all = movieSessionRepository.findAllByProjection(date, pageable);
        return ResponseEntity.ok(new ApiResponse(SUCCESS, true, all));
    }

    @Transactional
    public ResponseEntity<?> addMovieSession(MovieSessionDto movieSessionDto) {
        Integer extraTime = extraTimeBetweenSessionsRepository.findFirst();
        MovieAnnouncement movieAnnouncement;
        Hall hall;
        List<MovieSession> movieSessions = new ArrayList<>();
        List<MovieSession> movieSessionList = new ArrayList<>();
        try {
            try {
                movieAnnouncement = movieAnnouncementRepository.findById(movieSessionDto.getMovieAnnouncementId()).orElseThrow(() -> new ResourceNotFoundException("Movie announcement not found"));
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
            }
            for (ReservedHallDto reservedHallDto : movieSessionDto.getReservedHallDtoList()) {
                try {
                    hall = hallRepository.findById(reservedHallDto.getHallId()).orElseThrow(() -> new ResourceNotFoundException("Hall not found!"));
                } catch (Exception e) {
                    e.printStackTrace();
                    return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
                }

                if (reservedHallDto.getEndDate() != null) {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDate startDate = LocalDate.parse(reservedHallDto.getStartDate(), formatter);
                    LocalDate endDate = LocalDate.parse(reservedHallDto.getEndDate(), formatter);
                    long end = endDate.toEpochDay() - startDate.toEpochDay();
                    if (end > 0) {
                        for (int i = 0; i <= end; i++) {
                            LocalDate date = startDate.plusDays(i);
                            for (String startTime : reservedHallDto.getStartTimes()) {
                                LocalTime time = LocalTime.parse(startTime);
                                int minute = time.getMinute() + time.getHour() * 60;
                                Integer endTime = movieAnnouncement.getMovie().getDurationInMin() + minute + extraTime;
                                LocalTime endTimeSession = LocalTime.of(endTime / 60, endTime % 60);
                                boolean timeRangeExist = movieSessionRepository.isTimeRangeExist(reservedHallDto.getHallId(), date, time, endTimeSession);
                                if (timeRangeExist) {
                                    String res = "The hall is busy at " + time + " on " + date;
                                    return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(res, false, null));
                                }
                                double sessionAdditionalFeeInPercent = inPercentRepository.checkTime(time);
                                MovieSession movieSession = new MovieSession(hall, date, time, endTimeSession, sessionAdditionalFeeInPercent, movieAnnouncement);
                                movieSessions.add(movieSession);
                            }
                        }
                    }
                    movieSessionList = movieSessionRepository.saveAll(movieSessions);

                } else {
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDate startDate = LocalDate.parse(reservedHallDto.getStartDate(), formatter);
                    for (String startTime : reservedHallDto.getStartTimes()) {
                        LocalTime time = LocalTime.parse(startTime);
                        int minute = time.getMinute() + time.getHour() * 60;
                        Integer endTime = movieAnnouncement.getMovie().getDurationInMin() + minute + extraTime;
                        LocalTime endTimeSession = LocalTime.of(endTime / 60, endTime % 60);
                        boolean timeRangeExist = movieSessionRepository.isTimeRangeExist(reservedHallDto.getHallId(), startDate, time, endTimeSession);
                        if (timeRangeExist) {
                            String res = "The hall is busy at " + time + " on " + startDate;
                            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(res, false, null));
                        }
                        double sessionAdditionalFeeInPercent = inPercentRepository.checkTime(time);
                        MovieSession movieSession = new MovieSession(hall, startDate, time, endTimeSession, sessionAdditionalFeeInPercent, movieAnnouncement);
                        movieSessions.add(movieSession);
                    }
                    movieSessionList = movieSessionRepository.saveAll(movieSessions);
                }

            }
            return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, movieSessionList));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_SAVE, false, null));
        }
    }

    public ResponseEntity<?> deleteMovieSession(UUID sessionId) {
        if (sessionId == null || sessionId.equals(""))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            movieSessionRepository.deleteById(sessionId);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_DELETE, false, null));
        }
    }

    public ResponseEntity<?> getSessionAvailableSeat(UUID sessionId) {
        if (sessionId == null || sessionId.equals(""))
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            movieSessionRepository.findById(sessionId).orElseThrow(() -> new ResourceNotFoundException("Movie session not found!"));
            List<AvailableSeatProjection> availableSeats = movieSessionRepository.getAvailableSeat(sessionId);
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, availableSeats));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }
}
