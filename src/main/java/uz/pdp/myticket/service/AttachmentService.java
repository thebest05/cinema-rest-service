package uz.pdp.myticket.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.model.Attachment;
import uz.pdp.myticket.model.AttachmentContent;
import uz.pdp.myticket.projection.AttachmentContentDataProjection;
import uz.pdp.myticket.repository.AttachmentContentRepository;
import uz.pdp.myticket.repository.AttachmentRepository;
import uz.pdp.myticket.service.qrCodeService.QRCodeServiceImpl;

import java.io.IOException;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepo;

    @Autowired
    AttachmentContentRepository attachmentContentRepo;

    @Autowired
    QRCodeServiceImpl qrCodeService;

    public Page<Attachment> getAllAttachment(Integer page, Integer size) {
        Pageable pageable= PageRequest.of(page,size);
        return attachmentRepo.findAll(pageable);
    }

    public ResponseEntity getAttachmentContentById(UUID id) {
        Attachment attachmentById = attachmentRepo.findAttachmentById(id);
        AttachmentContentDataProjection attachmentContent = attachmentContentRepo.findAttachmentContentDataByAttachment(attachmentById);

        return ResponseEntity
                .ok()
                .contentType(MediaType.valueOf(attachmentById.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; fileName=\""+attachmentById.getFileName()+"\"")
                .body(attachmentContent.getData());
    }

    public Attachment saveAttachment(MultipartFile multipartFile) {
        try {
            Attachment attachment = attachmentRepo.save(new Attachment(multipartFile.getOriginalFilename(), multipartFile.getContentType(), multipartFile.getSize()));
            AttachmentContent attachmentContent = attachmentContentRepo.save(new AttachmentContent(multipartFile.getBytes(), attachment));
            return attachment;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteAttachment(UUID id) {
        Attachment attachmentById = attachmentRepo.findAttachmentById(id);
        AttachmentContent attachmentContentByAttachment = attachmentContentRepo.findAttachmentContentByAttachment(attachmentById);
        attachmentContentRepo.deleteById(attachmentContentByAttachment.getId());
        attachmentRepo.deleteById(id);
    }

    public ResponseEntity<?> getQrCode(String qrCode) {
        int WIDTH = 150;
        int HEIGHT = 150;
        byte[] generateQRCode = qrCodeService.generateQRCode(qrCode, WIDTH, HEIGHT);
       return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_PNG)
                .header(HttpHeaders.CONTENT_DISPOSITION,"attachment; fileName=\"QrCode\"")
                .body(generateQRCode);
    }
}
