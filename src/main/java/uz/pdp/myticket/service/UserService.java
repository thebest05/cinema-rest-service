package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import uz.pdp.myticket.dto.LoginDto;
import uz.pdp.myticket.dto.RegisterDto;
import uz.pdp.myticket.model.Role;
import uz.pdp.myticket.model.User;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.payload.JwtResponse;
import uz.pdp.myticket.repository.PermissionRepository;
import uz.pdp.myticket.repository.RoleRepository;
import uz.pdp.myticket.repository.UserRepository;
import uz.pdp.myticket.security.JWTProvider;


import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static uz.pdp.myticket.utils.Constants.*;


@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    JWTProvider jwtProvider;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PermissionRepository permissionRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MailSenderService mailSenderService;

    @Autowired
    ClientCookieService clientCookieService;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ResponseEntity<?> login(LoginDto loginDto, Errors errors, HttpServletRequest request) {
        ResponseEntity<ApiResponse> CONFLICT = getErrors(errors);
        if (CONFLICT != null) return CONFLICT;

        User userDetails = loadUserByUsername(loginDto.getUsername());
        if (!userDetails.isEnabled()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("You did not confirm your email. Please confirm your email.", false, null));
        }
        if (userDetails != null && passwordEncoder.matches(loginDto.getPassword(), userDetails.getPassword())) {
            String generateToken = jwtProvider.generateToken(userDetails.getUsername());
            String refreshToken = jwtProvider.generateRefreshToken(userDetails.getUsername());
            JwtResponse jwtResponse = new JwtResponse(generateToken, "Bearer ", refreshToken, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(), userDetails.getRoles());
            new Thread(() -> {
                clientCookieService.addTicketUserCart(request,userDetails);
            }).start();
            return ResponseEntity.ok(new ApiResponse(SUCCESS_LOGIN, true, jwtResponse));
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("Username or password invalid!", false, null));
        }
    }

    @Transactional
    public ResponseEntity<?> register(RegisterDto registerDto, Errors errors) {
        ResponseEntity<ApiResponse> CONFLICT = getErrors(errors);
        if (CONFLICT != null) return CONFLICT;

        if (userRepository.existsUserByEmailAndUsernameAndPhoneNumber(registerDto.getEmail(), registerDto.getUsername(), registerDto.getPhoneNumber())) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse("You are already registered!", false, null));
        }
        try {
            Role roleUser = roleRepository.findRoleByName("ROLE_USER").orElseThrow(() -> new ResourceNotFoundException("Role not found!"));
            Set<Role> roles = new HashSet<>();
            roles.add(roleUser);
            User saveUser = getSaveUser(registerDto, roles);

            checkVerificationEmail(saveUser);

            sendEmailVerificationLink(saveUser);
            return ResponseEntity.ok(new ApiResponse("Thank you. You have successfully registered. Verification code has been sent to your email. Please check your email (spam folder)", true, null));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }

    private void checkVerificationEmail(User saveUser) {
        Timer timer = new Timer();
        TimerTask checkUser = new TimerTask() {
            @Override
            public void run() {
                if (!saveUser.isActive()) {
                    saveUser.setEnabled(false);
                    userRepository.save(saveUser);
                }
            }
        };
        timer.schedule(checkUser, 30 * 60 * 1000);
    }

    private ResponseEntity<ApiResponse> getErrors(Errors errors) {
        Map<String, List<String>> error = new HashMap<>();
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                if (!error.containsKey(fieldError.getField())) {
                    error.put(fieldError.getField(), new ArrayList<>(Arrays.asList(fieldError.getDefaultMessage())));
                } else {
                    error.get(fieldError.getField()).add(fieldError.getDefaultMessage());
                }
            }
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(ERROR, false, null, error));
        }
        return null;
    }

    private User getSaveUser(RegisterDto registerDto, Set<Role> roles) {
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        if (registerDto.getLastName() != null) {
            user.setLastName(registerDto.getLastName());
        }
        user.setEmail(registerDto.getEmail());
        user.setUsername(registerDto.getUsername());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setPhoneNumber(registerDto.getPhoneNumber());
        user.setRoles(roles);
        user.setUserPermissions(Collections.EMPTY_SET);
        User saveUser = userRepository.save(user);
        return saveUser;
    }

    private void sendEmailVerificationLink(User saveUser) {
        new Thread(() -> {
            String sendSubject = "Verification email: uz-ticket.uz";
            String sendContext = "<h1>Hello " + saveUser.getFirstName() + "</h1>\n" +
                    "<br>" +
                    "<p><b>Thank you for registering on our website. Please confirm your email via this link:</b></p>" +
                    "<br>" +
                    "http://localhost:8080/api/verification-email/" + saveUser.getEmail();
            String sendTo = saveUser.getEmail();
            mailSenderService.sendEmail(sendTo, sendContext, sendSubject);
        }).start();
    }

    public ResponseEntity<?> verificationEmail(String email) {
        if (email == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            User user = userRepository.findUserByEmail(email).orElseThrow(() -> new UsernameNotFoundException("User not found!"));
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime registeredDateTime = user.getCreatedAt().toLocalDateTime();
            long between = ChronoUnit.SECONDS.between(now, registeredDateTime);
            if (between <= 86400) {
                user.setActive(true);
                User saveUser = userRepository.save(user);
                return ResponseEntity.ok(new ApiResponse("Email confirmed successfully", true, null));
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
    }


    public ResponseEntity<?> refreshToken(String refreshToken, HttpServletRequest httpServletRequest) {
        if (jwtProvider.validateToken(refreshToken, httpServletRequest)) {
            try {
            String usernameFromToken = jwtProvider.getUsernameFromToken(refreshToken);
            String accessToken = jwtProvider.generateToken(usernameFromToken);
            User user = userRepository.findByUsername(usernameFromToken).orElseThrow(() -> new UsernameNotFoundException("User not found!"));
                JwtResponse jwtResponse = new JwtResponse(accessToken, "Bearer ", refreshToken, user.getId(), usernameFromToken, user.getEmail(), user.getRoles());
           return ResponseEntity.ok(new ApiResponse(SUCCESS,true,jwtResponse));
            }catch (Exception e){
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ApiResponse("Please register first!",false,null));
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new ApiResponse("Please log in!",false,null));
    }


}
