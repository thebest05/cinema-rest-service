package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.model.ClientCookie;
import uz.pdp.myticket.model.Ticket;
import uz.pdp.myticket.model.User;
import uz.pdp.myticket.repository.ClientCookieRepository;
import uz.pdp.myticket.repository.TicketRepository;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@Service
public class ClientCookieService {

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    ClientCookieRepository cookieRepository;

    public void addTicketUserCart(HttpServletRequest request, User userDetails) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null){
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("cart_tickets")){
                    UUID cookieId=UUID.fromString(cookie.getValue());
                    List<UUID> ticketIds = cookieRepository.findClientCookieByCookieId(cookieId);
                    if (ticketIds.size() > 0) {
                    List<Ticket> ticketList = ticketRepository.findAllById(ticketIds);
                    for (Ticket ticket : ticketList) {
                        ticket.setUser(userDetails);
                        ticket.setCreatedBy(userDetails.getId());
                        ticket.setUpdatedBy(userDetails.getId());
                        ticketRepository.save(ticket);
                    }
                    List<ClientCookie> clientCookieList = cookieRepository.findClientCookiesByCookieId(cookieId);
                    cookieRepository.deleteAll(clientCookieList);
                    }
                    break;
                }
            }
        }
    }
}
