package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.dto.DistributorDto;
import uz.pdp.myticket.model.Attachment;
import uz.pdp.myticket.model.AttachmentContent;
import uz.pdp.myticket.model.Distributor;
import uz.pdp.myticket.repository.AttachmentContentRepository;
import uz.pdp.myticket.repository.AttachmentRepository;
import uz.pdp.myticket.repository.DistributorRepository;

import java.io.IOException;
import java.util.UUID;

@Service
public class DistributorService {

    @Autowired
    DistributorRepository distributorRepo;

    @Autowired
    AttachmentRepository attachmentRepo;

    @Autowired
    AttachmentContentRepository attachmentContentRepo;

    public Page<Distributor> getAllDistributor(Integer page, Integer size) {
        Pageable pageable = PageRequest.of(page,size);
        return distributorRepo.findAll(pageable);
    }

    public Distributor saveDistributor(MultipartFile multipartFile, DistributorDto distributorDto ) {
        try {
            Attachment attachment = attachmentRepo.save(new Attachment(multipartFile.getOriginalFilename(),
                    multipartFile.getContentType(), multipartFile.getSize()));
            AttachmentContent attachmentContent = attachmentContentRepo.save(new AttachmentContent(multipartFile.getBytes(), attachment));
            return distributorRepo.save(new Distributor(distributorDto.getName(), distributorDto.getDescription(), attachment));
        } catch (IOException e) {
            e.printStackTrace();
             return null;
        }


    }

    public void deleteDistributor(UUID id) {
        Distributor distributorById = distributorRepo.findDistributorById(id);
        AttachmentContent content = attachmentContentRepo.findAttachmentContentByAttachment(distributorById.getLogo());
        attachmentContentRepo.deleteById(content.getId());
        distributorRepo.deleteById(id);
    }

    public Distributor getDistributorById(UUID id) {
      return   distributorRepo.findDistributorById(id);
    }

    public Distributor editDistributor(DistributorDto distributorDto, UUID id) {
        Distributor distributorById = distributorRepo.findDistributorById(id);
        distributorById.setName(distributorDto.getName());
        distributorById.setDescription(distributorDto.getDescription());
        return distributorRepo.save(distributorById);
    }
}
