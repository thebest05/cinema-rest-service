package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.enums.PurchaseStatus;
import uz.pdp.myticket.model.CashBox;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.repository.CashBoxRepository;
import uz.pdp.myticket.repository.PurchaseRepository;
import uz.pdp.myticket.repository.TicketRepository;

import static uz.pdp.myticket.utils.Constants.SUCCESS;

@Service
public class DashboardService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    CashBoxRepository cashBoxRepository;




    public ResponseEntity<?> getIncome() {
        Double totalAmount = purchaseRepository.findPurchaseHistoriesByPurchaseStatus(PurchaseStatus.getPurchaseDisplayType("paid"));
        return ResponseEntity.ok(new ApiResponse(SUCCESS,true,totalAmount));
    }

    public ResponseEntity<?> getOutcome() {
        Double totalAmount = purchaseRepository.findPurchaseHistoriesByPurchaseStatus(PurchaseStatus.getPurchaseDisplayType("refund"));
        return ResponseEntity.ok(new ApiResponse(SUCCESS,true,totalAmount));
    }

    public ResponseEntity<?> getBalance() {
        CashBox cashBox = cashBoxRepository.findAll().get(0);
        return ResponseEntity.ok(new ApiResponse(SUCCESS,true,cashBox));
    }

    public ResponseEntity<?> getTicketsSold(){
        Long soldTicket = purchaseRepository.countSoldTicket(PurchaseStatus.getPurchaseDisplayType("paid"));
        return ResponseEntity.ok(new ApiResponse(SUCCESS,true,soldTicket));
    }

    public ResponseEntity<?> getRefundTicket() {
        Long refundTicket = purchaseRepository.countSoldTicket(PurchaseStatus.getPurchaseDisplayType("refund"));
        return ResponseEntity.ok(new ApiResponse(SUCCESS,true,refundTicket));
    }
}
