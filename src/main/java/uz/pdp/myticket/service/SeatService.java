package uz.pdp.myticket.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.dto.SeatDataDto;
import uz.pdp.myticket.dto.SeatDto;
import uz.pdp.myticket.model.PriceCategory;
import uz.pdp.myticket.model.Row;
import uz.pdp.myticket.model.Seat;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.SeatAllDataProjection;
import uz.pdp.myticket.repository.PriceCategoryRepository;
import uz.pdp.myticket.repository.RowRepository;
import uz.pdp.myticket.repository.SeatRepository;


import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class SeatService {

    @Autowired
    SeatRepository seatRepository;

    @Autowired
    RowRepository rowRepository;

    @Autowired
    PriceCategoryRepository priceCategoryRepo;

    public ResponseEntity<?> getAllSeat(UUID rowId, Integer page, Integer size) {
        if (rowId == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        Pageable pageable = PageRequest.of(page, size);
        Page<SeatAllDataProjection> seats = seatRepository.findSeatsByRowId(rowId, pageable);
        return ResponseEntity.ok(new ApiResponse(SUCCESS, true, seats));
    }

    @Transactional
    public ResponseEntity<?> addSeat(UUID rowId, SeatDto seatDto) {
        if (rowId == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        Row row;
        PriceCategory priceCategory;
        Integer count = 0;
        try {
            row = rowRepository.findById(rowId).orElseThrow(() -> new ResourceNotFoundException("Row not found!"));
            priceCategory = priceCategoryRepo.findById(seatDto.getPriceCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Price Category not found!"));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }

        for (Integer i = seatDto.getStartSeatNumber(); i <= (seatDto.getEndSeatNumber() != null ? seatDto.getEndSeatNumber() : seatDto.getStartSeatNumber()); i++) {
            boolean alreadyExist = seatRepository.existsSeatByNumberAndRow(i, row);
            if (!alreadyExist) {
                seatRepository.save(new Seat(i, priceCategory, row));
                count++;
            } else {
                List<UUID> seatByRowId = seatRepository.findSeatsByRowIdAndAndOrderByCreatedByIdDesc(rowId, count);
                seatRepository.deleteAllById(seatByRowId);
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse("Seat "+i+" is already in the "+row.getNumber()+"th row", false, null));
            }
        }
        return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, null));
    }

    public ResponseEntity<?> deleteSeat(UUID seatId) {
        if (seatId == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            seatRepository.deleteById(seatId);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_DELETE, false, null));
        }
    }

    public ResponseEntity<?> editData(UUID seatId, SeatDataDto seatDataDto) {
        if (seatId == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        Seat seat;
        try {
            PriceCategory priceCategory = priceCategoryRepo.findById(seatDataDto.getPriceCategoryId()).orElseThrow(() -> new ResourceNotFoundException("Price category not found!"));
            seat = seatRepository.findById(seatId).orElseThrow(() -> new ResourceNotFoundException("Seat not found!"));
            boolean alreadyExist = seatRepository.existsSeatByNumberAndRow(seatDataDto.getNumber(), seat.getRow());
            if (!alreadyExist) {
                seat.setNumber(seatDataDto.getNumber());
                seat.setPriceCategory(priceCategory);
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse("Seat "+seatDataDto.getNumber()+" is already in the "+seat.getRow().getNumber()+"th row", false, null));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }
        try {
            Seat editSeat = seatRepository.save(seat);
            return ResponseEntity.ok(new ApiResponse(SUCCESS_EDIT, true, editSeat));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_EDIT, false, null));
        }
    }

    public ResponseEntity<?> getSeatById(UUID seatId) {
        if (seatId == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse(ERROR, false, null));
        try {
            SeatAllDataProjection seatsById = seatRepository.findSeatsById(seatId).orElseThrow(() -> new ResourceNotFoundException("Seat not found!"));
            return ResponseEntity.ok(new ApiResponse(SUCCESS, true, seatsById));
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(e.getMessage(), false, null));
        }


    }
}
