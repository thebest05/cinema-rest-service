package uz.pdp.myticket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.myticket.model.Ticket;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.repository.TicketRepository;
import uz.pdp.myticket.utils.Constants;

import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@Service
public class CheckTicketByQRCodeService {

    @Autowired
    TicketRepository ticketRepository;

    public ResponseEntity<?> checkTicket(UUID id) {
        if (id == null || id.equals("")) return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(INVALID_TICKET,false,null));
        try {
            Ticket ticket = ticketRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Ticket not found!"));
            if (ticket.isValid()) {
                ticket.setValid(false);
                ticketRepository.save(ticket);
                return ResponseEntity.ok(new ApiResponse(VALID_TICKET,true,null));
            }else {
                return ResponseEntity.ok(new ApiResponse(INVALID_TICKET,false,null));
            }
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(OBJECT_NOT_FOUND,false,null));
        }
    }
}
