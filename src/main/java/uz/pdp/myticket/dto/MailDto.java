package uz.pdp.myticket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MailDto {
    private String sendFrom;
    private String sendTo;
    private String sendSubject;
    private String sendTemplate;
    private Map<String, Object> properties;
}
