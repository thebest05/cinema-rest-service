package uz.pdp.myticket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegisterDto {

    @NotEmpty(message = "Value must be entered")
    private String firstName;

    private String lastName;

    @Size(min = 2,message = "Username must be longer than 2 characters")
    @NotEmpty(message = "Value must be entered")
    @NotBlank(message = "There can be no space between the characters")
    private String username;

    @NotBlank(message = "The password cannot be empty")
    @Size(min = 8, message = "Password must be longer than 8 characters")
    private String password;

    @NotBlank(message = "Email cannot be empty")
    @Email(message = "You need to enter an email")
    private String email;

    @NotBlank(message = "The phone number cannot be empty")
    private String phoneNumber;
}
