package uz.pdp.myticket.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.myticket.template.AbsEntity;



@AllArgsConstructor
@NoArgsConstructor
@Data
public class DistributorDto  {

    private String name;
    private String description;
}
