package uz.pdp.myticket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.myticket.model.Row;
import uz.pdp.myticket.model.Seat;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class HallDto {
    private String name;
    private Double vipAdditionalFeeInPercent;
    private List<RowDto> rows;
}
