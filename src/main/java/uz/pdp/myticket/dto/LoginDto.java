package uz.pdp.myticket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LoginDto {


    @NotBlank(message = "Value must be entered")
    private String username;

    @NotBlank(message = "The password cannot be empty")
    private String password;

}
