package uz.pdp.myticket.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmailTicketDto {
    private UUID id;
    private String movieTitle;
    private UUID moviePosterImage;
    private String hallName;
    private Integer rowNumber;
    private Integer seatNumber;
    private Double price;
    private LocalDate startDate;
    private LocalDate purchaseDate;
    private LocalTime startTime;
    private String qrCode;
}
