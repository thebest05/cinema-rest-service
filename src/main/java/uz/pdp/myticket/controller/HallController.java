package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.HallDto;
import uz.pdp.myticket.model.Hall;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.service.HallService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/hall")
public class HallController {

    @Autowired
    HallService hallService;

    @GetMapping
    public ResponseEntity<?> getAllHalls(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ){
            return hallService.getAllHalls(page,size);
    }

    @GetMapping("/{hallId}")
    public ResponseEntity<?> getHallById(@PathVariable UUID hallId){
        return hallService.getHallById(hallId);
    }

    @PostMapping
    public ResponseEntity<?> addHall(@RequestBody HallDto hallDto){
        return hallService.addHall(hallDto);
    }

    @PutMapping("/{hallId}")
    public ResponseEntity<?> editHall(@PathVariable UUID hallId,@RequestBody HallDto hallDto){
        return hallService.editHall(hallId,hallDto);
    }

    @DeleteMapping("/{hallId}")
    public ResponseEntity<?> deleteHall(@PathVariable UUID hallId){
       return hallService.deleteHall(hallId);

    }
}
