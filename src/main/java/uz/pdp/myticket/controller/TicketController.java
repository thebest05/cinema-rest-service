package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.TicketDto;
import uz.pdp.myticket.service.TicketService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@RequestMapping("/api/ticket")
public class TicketController {

    @Autowired
    TicketService ticketService;

    @GetMapping
    public ResponseEntity<?> getAllTickets(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "100") Integer size
    ){
        return ticketService.getAllTickets(page,size);
    }

    @GetMapping("/get-my-ticket")
    public ResponseEntity<?> getMyTicket(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "100") Integer size
    ){
        return ticketService.getMYTicket(page,size);
    }

    @GetMapping("/download-ticket/{ticketId}")
    public ResponseEntity<?> downloadTicket(@PathVariable UUID ticketId){
        return ticketService.downloadTicket(ticketId);
    }

    @PostMapping("/create")
    public ResponseEntity<?> generateTicket(@RequestBody TicketDto ticketDto, HttpServletResponse response, HttpServletRequest request){
        return ticketService.generateTicket(ticketDto,response,request);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteTicket(@PathVariable UUID id){
        return ticketService.deleteTicket(id);
    }

}
