package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.PurchaseDto;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.service.PurchaseHistoryService;

import java.util.UUID;

@RestController
@RequestMapping("/api/purchase")
public class PurchaseHistoryController {

    @Autowired
    PurchaseHistoryService purchaseService;


    @GetMapping
    public ResponseEntity<?> purchaseTicket(){
        return purchaseService.purchaseTicket();
    }

    @GetMapping("/refund/{ticketId}")
    public ResponseEntity<?> refundTicket(@PathVariable UUID ticketId,@RequestParam(value = "isRefund",required = false,defaultValue = "false") boolean isRefund){
        return purchaseService.refundTicket(ticketId,isRefund);
    }

    @GetMapping("/paid")
    public ResponseEntity<?> getPurchasedTicket(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ){
        return purchaseService.getPurchasedTicket(page,size);
    }

    @GetMapping("/refund")
    public ResponseEntity<?> getRefundTicket(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ){
        return purchaseService.getRefundTicket(page,size);
    }
}
