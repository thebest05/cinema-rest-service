package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.MovieAnnouncementDto;
import uz.pdp.myticket.model.MovieAnnouncement;
import uz.pdp.myticket.service.MovieAnnouncementService;

import java.util.UUID;

@RestController
@RequestMapping("/api/announcement")
public class MovieAnnouncementController {

    @Autowired
    MovieAnnouncementService movieAnnouncementService;

    @GetMapping
    public ResponseEntity<?> getAllMovieAnnouncement(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ){
        return movieAnnouncementService.getAllMovieAnnouncement(page,size);
    }

    @PostMapping
    public ResponseEntity<?> addMovieAnnouncement(@RequestBody MovieAnnouncementDto movieAnnouncementDto){
        return movieAnnouncementService.addMovieAnnouncement(movieAnnouncementDto);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editMovieAnnouncement(@PathVariable UUID id, @RequestBody MovieAnnouncementDto movieAnnouncementDto){
        return movieAnnouncementService.editMovieAnnouncement(id,movieAnnouncementDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMovieAnnouncement(@PathVariable UUID id){
        return movieAnnouncementService.deleteMovieAnnouncement(id);
    }
}
