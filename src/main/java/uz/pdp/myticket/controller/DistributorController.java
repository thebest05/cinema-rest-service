package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.dto.DistributorDto;
import uz.pdp.myticket.model.Distributor;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.service.DistributorService;
import uz.pdp.myticket.utils.Constants;

import java.util.List;
import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@RestController
@RequestMapping("api/distributor")
public class DistributorController {

    @Autowired
    DistributorService distributorService;

    @GetMapping
    public ResponseEntity<?> getAllDistributor(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ) {
        Page<Distributor> distributors = distributorService.getAllDistributor(page, size);
        ApiResponse apiResponse = new ApiResponse(SUCCESS, true, distributors);
        return ResponseEntity.ok(apiResponse);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getDistributorById(@PathVariable UUID id) {
        Distributor distributor = distributorService.getDistributorById(id);
        return ResponseEntity.ok(new ApiResponse(SUCCESS, true, distributor));

    }

    @PostMapping
    public ResponseEntity<?> saveDistributor(@RequestPart("file") MultipartFile multipartFile,
                                             @RequestPart("json") DistributorDto distributorDto) {
        Distributor distributor1 = distributorService.saveDistributor(multipartFile, distributorDto);
        if (distributor1 != null) {
            return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, distributor1));
        } else {
            return ResponseEntity.ok(new ApiResponse(FAILED_TO_SAVE, false, null));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editDistributor(@PathVariable UUID id, @RequestBody DistributorDto distributorDto) {
        Distributor distributor = distributorService.editDistributor(distributorDto,id);
        return ResponseEntity.ok(new ApiResponse(SUCCESS_EDIT, true, distributor));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteDistributor(@PathVariable UUID id) {
        distributorService.deleteDistributor(id);
        return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
    }

}
