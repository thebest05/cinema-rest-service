package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.model.Attachment;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.projection.AttachmentContentDataProjection;
import uz.pdp.myticket.service.AttachmentService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/attachment")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @GetMapping
    public ResponseEntity<?> getAllAttachment(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "100") Integer size
    ){
        Page<Attachment> attachments = attachmentService.getAllAttachment(page, size);
        return ResponseEntity.ok(new ApiResponse("Success", true,attachments));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getAttachmentContentById(@PathVariable UUID id){
        return attachmentService.getAttachmentContentById(id);
    }

    @GetMapping("/qrcode")
    public ResponseEntity<?> getQRCode(@RequestParam(name = "qrcode") String qrCode){
        return attachmentService.getQrCode(qrCode);
    }

    @PostMapping
    public ResponseEntity<?> saveAttachment(@RequestPart("file")MultipartFile multipartFile){
       Attachment attachment = attachmentService.saveAttachment(multipartFile);
        if (attachment != null) {
            return ResponseEntity.ok(new ApiResponse("Successfully added", true, attachment));
        } else {
            return ResponseEntity.ok(new ApiResponse("Could not save", false, null));
        }
    }



    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteAttachment(@PathVariable UUID id){
        attachmentService.deleteAttachment(id);
        return ResponseEntity.ok(new ApiResponse("Successfully deleted", true, null));
    }
}
