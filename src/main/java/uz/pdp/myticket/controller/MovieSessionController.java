package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.MovieSessionDto;
import uz.pdp.myticket.service.MovieSessionService;

import java.time.LocalDate;
import java.util.UUID;

@RestController
@RequestMapping("/api/movie-session")
public class MovieSessionController {

    @Autowired
    MovieSessionService movieSessionService;

    @GetMapping
    public ResponseEntity<?> getAllMovieSession(
            @RequestParam(name = "date",required = false) String date,
            @RequestParam(name = "page",required = false) Integer page,
            @RequestParam(name = "size",required = false) Integer size){
        LocalDate localDate=null;
        if (date != null){
        localDate = LocalDate.parse(date);
        }
        return movieSessionService.getAllMovieSession(localDate,page,size);
    }


    @GetMapping("/available-seat/{sessionId}")
    public ResponseEntity<?> getSessionAvailableSeat(
            @PathVariable UUID sessionId
    ){
        return movieSessionService.getSessionAvailableSeat(sessionId);
    }
    @PostMapping
    public ResponseEntity<?> addMovieSession(@RequestBody MovieSessionDto movieSessionDto){
        return movieSessionService.addMovieSession(movieSessionDto);
    }

    @DeleteMapping("/{sessionId}")
    public ResponseEntity<?> deleteMovieSession(@PathVariable UUID sessionId){
        return movieSessionService.deleteMovieSession(sessionId);
    }
}
