package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.RowDto;
import uz.pdp.myticket.service.RowService;

import java.util.UUID;

@RestController
@RequestMapping("/api/row")
public class RowController {

    @Autowired
    RowService rowService;

    @GetMapping
    public ResponseEntity<?> getAllRows(
            @RequestParam(value = "hallId",required = false)UUID hallId,
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size){
       return rowService.getAllRows(hallId,page,size);
    }

    @GetMapping("/{rowId}")
    public ResponseEntity<?> getRowById(
            @PathVariable UUID rowId){
        return rowService.getRowById(rowId);
    }

    @PutMapping("/{rowId}")
    public ResponseEntity<?> editRow(@PathVariable UUID rowId, RowDto rowDto){
        return rowService.editRow(rowId,rowDto);
    }

    @PostMapping
    public ResponseEntity<?> addRow(@RequestParam(value = "hallId",required = false) UUID hallId, @RequestBody RowDto rowDto){
        return rowService.addRow(hallId,rowDto);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRow(@PathVariable UUID id){
        return rowService.deleteRow(id);
    }
}
