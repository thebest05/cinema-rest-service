package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.myticket.service.MailSenderService;

@RestController
@RequestMapping("/api/mail")
public class MailSenderController {

    @Autowired
    MailSenderService mailSenderService;

    @GetMapping
    public void sendEmail(){
        String context="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
                "<html xmlns=\"http://www.w3.org/1999/xhtml\"\n" +
                "      xmlns:v=\"urn:schemas-microsoft-com:vml\"\n" +
                "      xmlns:o=\"urn:schemas-microsoft-com:office:office\">\n" +
                "<head>\n" +
                "    <!--[if gte mso 9]><xml>\n" +
                "    <o:OfficeDocumentSettings>\n" +
                "        <o:AllowPNG/>\n" +
                "        <o:PixelsPerInch>96</o:PixelsPerInch>\n" +
                "    </o:OfficeDocumentSettings>\n" +
                "</xml><![endif]-->\n" +
                "    <meta http-equiv=\"Content-type\" content=\"text/html; charset=utf-8\" />\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\" />\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
                "    <meta name=\"format-detection\" content=\"date=no\" />\n" +
                "    <meta name=\"format-detection\" content=\"address=no\" />\n" +
                "    <meta name=\"format-detection\" content=\"telephone=no\" />\n" +
                "    <title>Email Template</title>\n" +
                "\n" +
                "\n" +
                "    <style type=\"text/css\" media=\"screen\">\n" +
                "        /* Linked Styles */\n" +
                "        body { padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none }\n" +
                "        a { color:#a88123; text-decoration:none }\n" +
                "        p { padding:0 !important; margin:0 !important }\n" +
                "\n" +
                "        /* Mobile styles */\n" +
                "    </style>\n" +
                "    <style media=\"only screen and (max-device-width: 480px), only screen and (max-width: 480px)\" type=\"text/css\">\n" +
                "        @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {\n" +
                "            div[class='mobile-br-5'] { height: 5px !important; }\n" +
                "            div[class='mobile-br-10'] { height: 10px !important; }\n" +
                "            div[class='mobile-br-15'] { height: 15px !important; }\n" +
                "            div[class='mobile-br-20'] { height: 20px !important; }\n" +
                "            div[class='mobile-br-25'] { height: 25px !important; }\n" +
                "            div[class='mobile-br-30'] { height: 30px !important; }\n" +
                "\n" +
                "            th[class='m-td'],\n" +
                "            td[class='m-td'],\n" +
                "            div[class='hide-for-mobile'],\n" +
                "            span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }\n" +
                "\n" +
                "            span[class='mobile-block'] { display: block !important; }\n" +
                "\n" +
                "            div[class='wgmail'] img { min-width: 320px !important; width: 320px !important; }\n" +
                "\n" +
                "            div[class='img-m-center'] { text-align: center !important; }\n" +
                "\n" +
                "            div[class='fluid-img'] img,\n" +
                "            td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }\n" +
                "\n" +
                "            table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }\n" +
                "            td[class='td'] { width: 100% !important; min-width: 100% !important; }\n" +
                "\n" +
                "            table[class='center'] { margin: 0 auto; }\n" +
                "\n" +
                "            td[class='column-top'],\n" +
                "            th[class='column-top'],\n" +
                "            td[class='column'],\n" +
                "            th[class='column'] { float: left !important; width: 100% !important; display: block !important; }\n" +
                "\n" +
                "            td[class='content-spacing'] { width: 15px !important; }\n" +
                "\n" +
                "            div[class='h2'] { font-size: 44px !important; line-height: 48px !important; }\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body class=\"body\" style=\"padding:0 !important; margin:0 !important; display:block !important; background:#1e1e1e; -webkit-text-size-adjust:none\">\n" +
                "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#1e1e1e\">\n" +
                "    <tr>\n" +
                "        <td align=\"center\" valign=\"top\">\n" +
                "            <!-- Top -->\n" +
                "            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#161616\">\n" +
                "                <tr>\n" +
                "                    <td align=\"center\" valign=\"top\">\n" +
                "                        <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">\n" +
                "                            <tr>\n" +
                "                                <td class=\"td\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; width:600px; min-width:600px; Margin:0\" width=\"600\">\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                        <tr>\n" +
                "                                            <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                            <td>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td>\n" +
                "                                                            <div class=\"text-header\" style=\"color:#666666; font-family:Arial, sans-serif; min-width:auto !important; font-size:12px; line-height:16px; text-align:left\">\n" +
                "                                                                <a href=\"#\" target=\"_blank\" class=\"link-1\" style=\"color:#666666; text-decoration:none\"><span class=\"link-1\" style=\"color:#666666; text-decoration:none\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/ZBZBRNHoQoCRD4F8SSN0_ico_webversion.jpg\" border=\"0\" width=\"14\" height=\"16\" alt=\"\" style=\"vertical-align: middle;\" />&nbsp; Web Version</span></a>\n" +
                "                                                            </div>\n" +
                "                                                        </td>\n" +
                "                                                        <td>\n" +
                "                                                            <div class=\"text-header-1\" style=\"color:#666666; font-family:Arial, sans-serif; min-width:auto !important; font-size:12px; line-height:16px; text-align:right\">\n" +
                "                                                                <a href=\"#\" target=\"_blank\" class=\"link-1\" style=\"color:#666666; text-decoration:none\"><span class=\"link-1\" style=\"color:#666666; text-decoration:none\"><img src=\"images/ico_forward.jpg\" border=\"0\" width=\"14\" height=\"16\" alt=\"\" style=\"vertical-align: middle;\" />&nbsp; Forward</span></a>\n" +
                "                                                            </div>\n" +
                "                                                        </td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                            </td>\n" +
                "                                            <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                        </tr>\n" +
                "                                    </table>\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                        </table>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <!-- END Top -->\n" +
                "\n" +
                "            <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"mobile-shell\">\n" +
                "                <tr>\n" +
                "                    <td class=\"td\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; width:600px; min-width:600px; Margin:0\" width=\"600\">\n" +
                "                        <!-- Header -->\n" +
                "                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                            <tr>\n" +
                "                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                <td>\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"30\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                    <div class=\"img-center\" style=\"font-size:0pt; line-height:0pt; text-align:center\"><a href=\"#\" target=\"_blank\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/3Bvp1prkTtGdFMgsCg6p_logo.jpg\" border=\"0\" width=\"203\" height=\"27\" alt=\"\" /></a></div>\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"30\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                    <div class=\"hide-for-mobile\">\n" +
                "                                        <div class=\"text-nav\" style=\"color:#ffffff; font-family:Arial, sans-serif; min-width:auto !important; font-size:12px; line-height:22px; text-align:center\">\n" +
                "                                            <a href=\"#\" target=\"_blank\" class=\"link-white\" style=\"color:#ffffff; text-decoration:none\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none\">HOME</span></a>\n" +
                "                                            &nbsp;&nbsp;|&nbsp;&nbsp;\n" +
                "                                            <a href=\"#\" target=\"_blank\" class=\"link-white\" style=\"color:#ffffff; text-decoration:none\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none\">NEW PRODUCTS</span></a>\n" +
                "                                            &nbsp;&nbsp;|&nbsp;&nbsp;\n" +
                "                                            <a href=\"#\" target=\"_blank\" class=\"link-white\" style=\"color:#ffffff; text-decoration:none\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none\">CATALOGUE</span></a>\n" +
                "                                            &nbsp;&nbsp;|&nbsp;&nbsp;\n" +
                "                                            <a href=\"#\" target=\"_blank\" class=\"link-white\" style=\"color:#ffffff; text-decoration:none\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none\">CONTACT US</span></a>\n" +
                "                                        </div>\n" +
                "                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"20\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                    </div>\n" +
                "                                </td>\n" +
                "                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                            </tr>\n" +
                "                        </table>\n" +
                "                        <!-- END Header -->\n" +
                "\n" +
                "                        <!-- Main -->\n" +
                "                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                            <tr>\n" +
                "                                <td>\n" +
                "                                    <!-- Head -->\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#d2973b\">\n" +
                "                                        <tr>\n" +
                "                                            <td>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"27\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/JJxrFRyVRr20CJD3pOx9_top_left.jpg\" border=\"0\" width=\"27\" height=\"27\" alt=\"\" /></td>\n" +
                "                                                        <td>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" height=\"3\" bgcolor=\"#e6ae57\">&nbsp;</td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"24\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                        </td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"27\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/SNcoUN5kSfCDagqSBEZ4_top_right.jpg\" border=\"0\" width=\"27\" height=\"27\" alt=\"\" /></td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"3\" bgcolor=\"#e6ae57\"></td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"10\"></td>\n" +
                "                                                        <td>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"15\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                            <div class=\"h3-2-center\" style=\"color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:20px; line-height:26px; text-align:center; letter-spacing:5px\">LOREM IPSUM</div>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"5\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                            <div class=\"h2\" style=\"color:#ffffff; font-family:Georgia, serif; min-width:auto !important; font-size:60px; line-height:64px; text-align:center\">\n" +
                "                                                                <em>Good News!</em>\n" +
                "                                                            </div>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"35\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                        </td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"10\"></td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"3\" bgcolor=\"#e6ae57\"></td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                            </td>\n" +
                "                                        </tr>\n" +
                "                                    </table>\n" +
                "                                    <!-- END Head -->\n" +
                "\n" +
                "                                    <!-- Body -->\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#ffffff\">\n" +
                "                                        <tr>\n" +
                "                                            <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                            <td>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"35\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                <div class=\"h3-1-center\" style=\"color:#1e1e1e; font-family:Georgia, serif; min-width:auto !important; font-size:20px; line-height:26px; text-align:center\">Your order has shipped! To track your order or make any changes please click the button below.</div>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"20\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                <!-- Button -->\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td align=\"center\">\n" +
                "                                                            <table width=\"210\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td align=\"center\" bgcolor=\"#d2973b\">\n" +
                "                                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"15\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"50\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "                                                                                </td>\n" +
                "                                                                                <td bgcolor=\"#d2973b\">\n" +
                "                                                                                    <div class=\"text-btn\" style=\"color:#ffffff; font-family:Arial, sans-serif; min-width:auto !important; font-size:16px; line-height:20px; text-align:center\">\n" +
                "                                                                                        <a href=\"#\" target=\"_blank\" class=\"link-white\" style=\"color:#ffffff; text-decoration:none\"><span class=\"link-white\" style=\"color:#ffffff; text-decoration:none\">MY ORDER</span></a>\n" +
                "                                                                                    </div>\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"15\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                    </td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                        </td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                                <!-- END Button -->\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"40\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <th class=\"column-top\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0\" valign=\"top\" width=\"270\">\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td>\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                                <td>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                    <div class=\"text-1\" style=\"color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left\">\n" +
                "                                                                                        <strong>SHIPPING ADDRESS:</strong>\n" +
                "                                                                                    </div>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#fafafa\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                                <td>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                    <div class=\"text\" style=\"color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left\">\n" +
                "                                                                                        <strong>Jane Doe</strong>\n" +
                "                                                                                        <br />\n" +
                "                                                                                        123 Street | Victoria, BC\n" +
                "                                                                                        <br />\n" +
                "                                                                                        Canada\n" +
                "                                                                                        <br />\n" +
                "                                                                                        1(250)222-2232\n" +
                "                                                                                    </div>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"15\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                    </td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                        </th>\n" +
                "                                                        <th class=\"column-top\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0\" valign=\"top\" width=\"20\">\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td><div style=\"font-size:0pt; line-height:0pt;\" class=\"mobile-br-15\"></div>\n" +
                "                                                                    </td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                        </th>\n" +
                "                                                        <th class=\"column-top\" style=\"font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0\" valign=\"top\" width=\"270\">\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td>\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                                <td>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                    <div class=\"text-1\" style=\"color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left\">\n" +
                "                                                                                        <strong>ORDER NUMBER:</strong> <span style=\"color: #1e1e1e;\">A80SD99</span>\n" +
                "                                                                                    </div>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"20\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#f4f4f4\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                                <td>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                    <div class=\"text-1\" style=\"color:#d2973b; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left\">\n" +
                "                                                                                        <strong>DATE SHIPPED:</strong>\n" +
                "                                                                                    </div>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#fafafa\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                                <td>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"10\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                    <div class=\"text\" style=\"color:#1e1e1e; font-family:Arial, sans-serif; min-width:auto !important; font-size:14px; line-height:20px; text-align:left\">\n" +
                "                                                                                        January 12, 2016\n" +
                "                                                                                    </div>\n" +
                "                                                                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"15\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                                                </td>\n" +
                "                                                                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                    </td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                        </th>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"35\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                <div class=\"h5-center\" style=\"color:#1e1e1e; font-family:Georgia, serif; min-width:auto !important; font-size:16px; line-height:26px; text-align:center\">\n" +
                "                                                    <em>\n" +
                "                                                        Thank you for your business.\n" +
                "                                                        <br />\n" +
                "                                                        Please <a href=\"#\" target=\"_blank\" class=\"link-u\" style=\"color:#a88123; text-decoration:underline\"><span class=\"link-u\" style=\"color:#a88123; text-decoration:underline\">contact us</span></a> with any questions regarding your order.\n" +
                "                                                    </em>\n" +
                "                                                </div>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"35\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                            </td>\n" +
                "                                            <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                        </tr>\n" +
                "                                    </table>\n" +
                "                                    <!-- END Body -->\n" +
                "\n" +
                "                                    <!-- Foot -->\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" bgcolor=\"#d2973b\">\n" +
                "                                        <tr>\n" +
                "                                            <td>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"3\" bgcolor=\"#e6ae57\"></td>\n" +
                "                                                        <td>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"30\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                            <div class=\"h3-1-center\" style=\"color:#1e1e1e; font-family:Georgia, serif; min-width:auto !important; font-size:20px; line-height:26px; text-align:center\">\n" +
                "                                                                <em>Follow Us</em>\n" +
                "                                                            </div>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"15\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "\n" +
                "                                                            <!-- Socials -->\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td align=\"center\">\n" +
                "                                                                        <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                            <tr>\n" +
                "                                                                                <td class=\"img-center\" style=\"font-size:0pt; line-height:0pt; text-align:center\" width=\"38\"><a href=\"#\" target=\"_blank\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/PZeWpIm2TkSqtS6i07xE_ico_facebook.jpg\" border=\"0\" width=\"28\" height=\"28\" alt=\"\" /></a></td>\n" +
                "                                                                                <td class=\"img-center\" style=\"font-size:0pt; line-height:0pt; text-align:center\" width=\"38\"><a href=\"#\" target=\"_blank\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/hAIPhWl2SB2cL0Atc4lB_ico_twitter.jpg\" border=\"0\" width=\"28\" height=\"28\" alt=\"\" /></a></td>\n" +
                "                                                                                <td class=\"img-center\" style=\"font-size:0pt; line-height:0pt; text-align:center\" width=\"38\"><a href=\"#\" target=\"_blank\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/NrXUpqcRQwKnJKzLkqS1_ico_instagram.jpg\" border=\"0\" width=\"28\" height=\"28\" alt=\"\" /></a></td>\n" +
                "                                                                                <td class=\"img-center\" style=\"font-size:0pt; line-height:0pt; text-align:center\" width=\"38\"><a href=\"#\" target=\"_blank\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/VaewiS8gT5ClCCR9vAO1_ico_pinterest.jpg\" border=\"0\" width=\"28\" height=\"28\" alt=\"\" /></a></td>\n" +
                "                                                                            </tr>\n" +
                "                                                                        </table>\n" +
                "                                                                    </td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                            <!-- END Socials -->\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"15\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                        </td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"3\" bgcolor=\"#e6ae57\"></td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                                <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                    <tr>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"27\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/nK8bYazcQWGAQt8sAH2g_bot_left.jpg\" border=\"0\" width=\"27\" height=\"27\" alt=\"\" /></td>\n" +
                "                                                        <td>\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"24\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                                            <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                                                                <tr>\n" +
                "                                                                    <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" height=\"3\" bgcolor=\"#e6ae57\">&nbsp;</td>\n" +
                "                                                                </tr>\n" +
                "                                                            </table>\n" +
                "                                                        </td>\n" +
                "                                                        <td class=\"img\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"27\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/v9RanaDRM2FzjQNT9PwV_bot_right.jpg\" border=\"0\" width=\"27\" height=\"27\" alt=\"\" /></td>\n" +
                "                                                    </tr>\n" +
                "                                                </table>\n" +
                "                                            </td>\n" +
                "                                        </tr>\n" +
                "                                    </table>\n" +
                "                                    <!-- END Foot -->\n" +
                "                                </td>\n" +
                "                            </tr>\n" +
                "                        </table>\n" +
                "                        <!-- END Main -->\n" +
                "\n" +
                "                        <!-- Footer -->\n" +
                "                        <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n" +
                "                            <tr>\n" +
                "                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                                <td>\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"30\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                    <div class=\"text-footer\" style=\"color:#666666; font-family:Arial, sans-serif; min-width:auto !important; font-size:12px; line-height:18px; text-align:center\">\n" +
                "                                        East Pixel Bld. 99,<span class=\"mobile-block\"></span> Creative City 9000,<span class=\"mobile-block\"></span> Republic of Design\n" +
                "                                        <br />\n" +
                "                                        <a href=\"http://www.YourSiteName.com\" target=\"_blank\" class=\"link-1\" style=\"color:#666666; text-decoration:none\"><span class=\"link-1\" style=\"color:#666666; text-decoration:none\">www.YourSiteName.com</span></a>\n" +
                "                                        <span class=\"mobile-block\"><span class=\"hide-for-mobile\">|</span></span>\n" +
                "                                        <a href=\"mailto:email@yoursitename.com\" target=\"_blank\" class=\"link-1\" style=\"color:#666666; text-decoration:none\"><span class=\"link-1\" style=\"color:#666666; text-decoration:none\">email@yoursitename.com</span></a>\n" +
                "                                        <span class=\"mobile-block\"><span class=\"hide-for-mobile\">|</span></span>\n" +
                "                                        Phone: <a href=\"tel:+1655606605\" target=\"_blank\" class=\"link-1\" style=\"color:#666666; text-decoration:none\"><span class=\"link-1\" style=\"color:#666666; text-decoration:none\">+1 (655) 606-605</span></a>\n" +
                "                                    </div>\n" +
                "                                    <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\"><tr><td height=\"30\" class=\"spacer\" style=\"font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%\">&nbsp;</td></tr></table>\n" +
                "\n" +
                "                                </td>\n" +
                "                                <td class=\"content-spacing\" style=\"font-size:0pt; line-height:0pt; text-align:left\" width=\"20\"></td>\n" +
                "                            </tr>\n" +
                "                        </table>\n" +
                "                        <!-- END Footer -->\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <div class=\"wgmail\" style=\"font-size:0pt; line-height:0pt; text-align:center\"><img src=\"https://d1pgqke3goo8l6.cloudfront.net/oD2XPM6QQiajFKLdePkw_gmail_fix.gif\" width=\"600\" height=\"1\" style=\"min-width:600px\" alt=\"\" border=\"0\" /></div>\n" +
                "        </td>\n" +
                "    </tr>\n" +
                "</table>\n" +
                "</body>\n" +
                "</html>";
        String email="abduraximovazizjon610@gmail.com";
        String subject="Ticket purchased";
       mailSenderService.sendEmail(email,context,subject);
    }


}
