package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.dto.CastDto;
import uz.pdp.myticket.model.Cast;
import uz.pdp.myticket.payload.ApiResponse;
import uz.pdp.myticket.service.CastService;
import uz.pdp.myticket.utils.Constants;

import java.util.List;
import java.util.UUID;

import static uz.pdp.myticket.utils.Constants.*;

@RestController
@RequestMapping("/api/cast")
public class CastController {

    @Autowired
    CastService castService;

    @GetMapping
    public ResponseEntity<?> getAllCast(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "100") Integer size) {
        Page<Cast> castList = castService.getAllCast(page, size);
        return ResponseEntity.ok(new ApiResponse(SUCCESS, true, castList));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCastById(@PathVariable UUID id) {
        Cast cast = castService.getCastById(id);
        return ResponseEntity.ok(new ApiResponse(SUCCESS, true, cast));
    }

    @PostMapping
    public ResponseEntity<?> addCast(
            @RequestPart(name = "file") MultipartFile file,
            @RequestPart(name = "json") CastDto castDto
    ) {
        Cast addCast = castService.addCast(file, castDto);
        if (addCast != null) {
            return ResponseEntity.ok(new ApiResponse(SUCCESS_SAVE, true, null));
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_SAVE, false, null));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> editCast(@PathVariable UUID id, @RequestBody CastDto castDto) {
        Cast cast = castService.edit(id, castDto);
        if (cast != null) {
            return ResponseEntity.ok(new ApiResponse(SUCCESS_EDIT, true, null));
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponse(FAILED_TO_EDIT, false, null));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCast(@PathVariable UUID id) {
        castService.deleteCast(id);
        return ResponseEntity.ok(new ApiResponse(SUCCESS_DELETE, true, null));
    }
}
