package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.myticket.service.DashboardService;

@RestController
@RequestMapping("/api/dashboard")
public class DashboardController {
    @Autowired
    DashboardService dashboardService;

    @GetMapping("/income")
    public ResponseEntity<?> getIncome(){
        return dashboardService.getIncome();
    }

    @GetMapping("/outcome")
    public ResponseEntity<?> getOutcome(){
        return dashboardService.getOutcome();
    }

    @GetMapping("/get-balance")
    public ResponseEntity<?> getBalance(){
        return dashboardService.getBalance();
    }

    @GetMapping("/sold-ticket")
    public ResponseEntity<?> getTicketsSold(){
        return dashboardService.getTicketsSold();
    }

    @GetMapping("/refund-ticket")
    public ResponseEntity<?> getRefundTicket(){
        return dashboardService.getRefundTicket();
    }
}
