package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.SeatDataDto;
import uz.pdp.myticket.dto.SeatDto;
import uz.pdp.myticket.service.SeatService;

import java.util.UUID;

@RestController
@RequestMapping("/api/seat")
public class SeatController {

    @Autowired
    SeatService seatService;

    @GetMapping
    public ResponseEntity<?> getAllSeat(
            @RequestParam(value = "rowId") UUID rowId,
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "100") Integer size) {
        return seatService.getAllSeat(rowId,page,size);
    }

    @GetMapping("/{seatId}")
    public ResponseEntity<?> getSeatById(@PathVariable UUID seatId) {
        return seatService.getSeatById(seatId);
    }

    @PostMapping
    public ResponseEntity<?> addSeat(@RequestParam("rowId") UUID rowId, @RequestBody SeatDto seatDto) {
        return seatService.addSeat(rowId, seatDto);
    }

    @PutMapping("/{seatId}")
    public ResponseEntity<?> editSeat(@PathVariable UUID seatId, @RequestBody SeatDataDto seatDataDto) {
        return seatService.editData(seatId, seatDataDto);
    }

    @DeleteMapping("/{seatId}")
    public ResponseEntity<?> deleteSeat(@PathVariable UUID seatId) {
        return seatService.deleteSeat(seatId);
    }
}
