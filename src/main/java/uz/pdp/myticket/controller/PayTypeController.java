package uz.pdp.myticket.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import uz.pdp.myticket.service.PayTypeService;

import java.util.UUID;

@RestController
@RequestMapping("/api/pay-type")
public class PayTypeController {

    @Autowired
    PayTypeService payTypeService;

    @GetMapping
    public ResponseEntity<?> getAllPayType(
            @RequestParam(name = "page",required = false,defaultValue = "0") Integer page,
            @RequestParam(name = "size",required = false,defaultValue = "100") Integer size
    ){
        return payTypeService.getAllPayType(page,size);
    }

    @GetMapping("/{payTypeId}")
    public ResponseEntity<?> getPayTypeById(@PathVariable UUID payTypeId){
        return payTypeService.getPayTypeById(payTypeId);
    }

    @PostMapping
    public ResponseEntity<?> addPayType(@RequestPart("file")  MultipartFile file, @RequestPart("name") String name){
        return payTypeService.addPayType(file,name);
    }

    @DeleteMapping("/{payTypeId}")
    public ResponseEntity<?> deletePayType(@PathVariable UUID payTypeId){
        return payTypeService.deletePayType(payTypeId);
    }
}
