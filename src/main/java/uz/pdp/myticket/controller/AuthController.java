package uz.pdp.myticket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import uz.pdp.myticket.dto.LoginDto;
import uz.pdp.myticket.dto.RegisterDto;
import uz.pdp.myticket.security.JWTProvider;
import uz.pdp.myticket.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    UserService userService;


    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginDto loginDto, Errors errors,HttpServletRequest request){
        return userService.login(loginDto,errors,request);
    }

//    @GetMapping("/login/google")
//    public ResponseEntity<?> loginWithGoogle(OAuth2AuthenticationToken authentication){
//        return userService.loginWithGoogle(authentication);
//    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterDto registerDto,Errors errors){
        return userService.register(registerDto,errors);
    }

    @GetMapping("/verification-email/{email}")
    public ResponseEntity<?> verificationEmail(@PathVariable String email){
        return userService.verificationEmail(email);
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<?> refreshToken(@RequestParam String refreshToken, HttpServletRequest httpServletRequest){

        return userService.refreshToken(refreshToken,httpServletRequest);
    }


}
