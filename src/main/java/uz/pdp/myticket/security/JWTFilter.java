package uz.pdp.myticket.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.OncePerRequestFilter;
import uz.pdp.myticket.repository.UserRepository;
import uz.pdp.myticket.service.UserService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTFilter extends OncePerRequestFilter {
    @Autowired
    JWTProvider jwtProvider;
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        UserDetails userDetails = getUserDetails(httpServletRequest);

        if (userDetails != null) {
            if (userDetails.isEnabled()
                    && userDetails.isAccountNonExpired()
                    && userDetails.isAccountNonLocked()
                    && userDetails.isCredentialsNonExpired()) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities());
//                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }



    public UserDetails getUserDetails(HttpServletRequest httpServletRequest) {
        try {

            String tokenClient = httpServletRequest.getHeader("Authorization");

            if (tokenClient != null) {

                if (tokenClient.startsWith("Bearer ")) {

                    tokenClient = tokenClient.substring(7);

                    if (jwtProvider.validateToken(tokenClient, httpServletRequest)) {
                        String usernameFromToken = jwtProvider.getUsernameFromToken(tokenClient);
                        return userService.loadUserByUsername(usernameFromToken);
                    }
                }
//                else if (tokenClient.startsWith("Basic ")) {
//                    String[] userNameAndPassword = getUserNameAndPassword(tokenClient);
//                    UserDetails userDetails = userService.loadUserByUsername(userNameAndPassword[0]);
//                    if (userDetails != null && passwordEncoder.matches(userNameAndPassword[1], userDetails.getPassword())) {
//                        return userDetails;
//                    }
//                }
            }
        } catch (Exception ignored) {
        }
        return null;
    }
}

//    public String[] getUserNameAndPassword(String token) {
//        // Authorization: Basic base64credentials
//        String base64Credentials = token.substring("Basic".length()).trim();
//        byte[] credDecoded = Base64.getDecoder().decode(base64Credentials);
//        String credentials = new String(credDecoded, StandardCharsets.UTF_8);
//        // credentials = username:password
//        return credentials.split(":", 2);
//    }




