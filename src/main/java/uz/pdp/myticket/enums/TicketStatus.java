package uz.pdp.myticket.enums;

public enum TicketStatus {
    NEW,
    PURCHASED,
    REFUNDED
}
