package uz.pdp.myticket.enums;

import java.util.Arrays;
import java.util.Optional;

public enum UserRole {
    ROLE_ADMIN("admin"),
    ROLE_USER("user"),
    ROLE_SUPPERADMIN("supperadmin"),
    ROLE_OTHER("other");

    final String displayName;

    UserRole(String displayName) {
        this.displayName = displayName;
    }

    public static CastType getCastDisplayType(String displayName){
        Optional<CastType> castType1 = Arrays.stream(CastType.values()).findFirst().filter(castType -> castType.displayName.equals(displayName.toLowerCase()));
        return castType1.orElse(CastType.CAST_OTHER);

    }
}
