package uz.pdp.myticket.enums;

import java.util.Arrays;
import java.util.Optional;

public enum PurchaseStatus {
    PURCHASE_PAID("paid"),
    PURCHASE_REFUNDED("refund"),
    OTHER("other");

    final String displayName;

    PurchaseStatus(String displayName) {
        this.displayName = displayName;
    }

    public static PurchaseStatus getPurchaseDisplayType(String displayName) {
        Optional<PurchaseStatus> purchaseStatus = Arrays.stream(PurchaseStatus.values()).findFirst().filter(purchaseStatus1 -> purchaseStatus1.displayName.equals(displayName.toLowerCase()));
        return purchaseStatus.orElse(PurchaseStatus.OTHER);
    }
}
